#pragma GCC diagnostic ignored "-Wignored-attributes"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define MAX_VECTOR_SIZE 512

// Quartic Solver
#include "QuarticSolver.h"
#include "QuarticSolver_MathChanges.h"
#include "QuarticSolverNew.h"
#include "QuarticSolverCacheline.h"
#include "QuarticSolverCachelineGCC.h"

#ifdef VTUNE
#include "ittnotify.h"
#endif //VTUNE

#include "vectype.h"
#include "randomDataGenerator.h"

// STL
#include <random>
#include <vector>
#include <iostream>
#include <string>
#include <typeinfo>
#include <chrono>

#include "debug.h"

uint64_t nIterations = NITERATIONS;
uint64_t nPhotons = NPHOTONS;
uint32_t chunkSize = 1000;

#define VECTOR_TYPE Vec16f
#define FLOAT_TYPE float
#define DIM 16

#define PARA 16

// Make an instance of the quartic solver
Rich::Rec::QuarticSolver qSolver;
RichNew::Rec::QuarticSolverNew qSolverNew;
Rich::Rec::QuarticSolverNewton newtonSolver;

RichCacheline::Rec::QuarticSolverCacheline<VECTOR_TYPE, FLOAT_TYPE>qSolverCacheline;
RichCachelineGCC::Rec::QuarticSolverCachelineGCC qSolverCachelineGCC;

void solve( PhotonData& data )
{
  qSolver.solve<FLOAT_TYPE>( data.emissPnt, 
                       data.centOfCurv, 
                       data.virtDetPoint,
                       data.radius, 
                       data.sphReflPoint );
}

void solveNewton( PhotonData& data )
{
  newtonSolver.solve<FLOAT_TYPE>( data.emissPnt, 
                       data.centOfCurv, 
                       data.virtDetPoint,
                       data.radius, 
                       data.sphReflPoint );
}

void solveNew( VECTYPE::PhotonReflection<FLOAT_TYPE>& data )
{
  qSolverCachelineGCC.solve<FLOAT_TYPE, FLOAT_TYPE, DIM>(data);
}

void solveClassic( PhotonData::Vector & dataV )
{
#ifdef DEBUG_RICH
  std::cout << "Solving Quartic Equation for "
            << typeid(TYPE).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  for ( auto& data : dataV ) { solve(data); }
}

void solveClassicNewtonParallel( PhotonData::Vector & dataV )
{
#ifdef DEBUG_RICH
  std::cout << "Solving Quartic Equation for "
            << typeid(TYPE).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  const auto end = dataV.end();
  #pragma omp parallel for schedule(static, chunkSize)
  for ( auto it = dataV.begin(); it < end; ++it) {
      solveNewton(*it); 
  }
}

void solveClassicParallel( PhotonData::Vector & dataV )
{
#ifdef DEBUG_RICH
  std::cout << "Solving Quartic Equation for "
            << typeid(TYPE).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  const auto end = dataV.end();
  #pragma omp parallel for schedule(static, chunkSize)
  for ( auto it = dataV.begin(); it < end; ++it) {
      solve(*it); 
  }
}

void solveNew(  typename VECTYPE::PhotonReflections<FLOAT_TYPE, DIM> & dataV )
{
#ifdef DEBUG_RICH
    std::cout << "Solving (new) Quartic Equation for "
            << typeid(TYPE).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  auto end = dataV.end();
  #pragma omp parallel for
  for (auto it = dataV.begin(); it < end; ++it) {
    solveNew(*it);
  }
  // for ( const auto& data : dataV ) { solveNew<TYPE>(data); }
}

//void solveCacheline( VECTYPE::PhotonReflection<FLOAT_TYPE,DIM>& data )
//{
//  qSolverCacheline.solve( data );
//}

void solveCacheline( typename VECTYPE::PhotonReflections<FLOAT_TYPE, DIM> & dataV )
{
#ifdef DEBUG_RICH
    std::cout << "Solving (new) Quartic Equation for "
            << typeid(VECT).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  auto end = dataV.end();
  //default(none) shared(dataV)
  qSolverCacheline.solve(dataV, chunkSize);
//#pragma omp parallel for schedule(static, chunkSize)
//  for (auto it = dataV.begin(); it < end; ++it) {
//    solveCacheline(*it);
//  }
  // for ( const auto& data : dataV ) { solveNew<TYPE>(data); }
}

inline void solveCachelineGCC( VECTYPE::PhotonReflection<FLOAT_TYPE,DIM>& data )
{
  qSolverCachelineGCC.solve<FLOAT_TYPE, FLOAT_TYPE, 16>( data ); 
}

void solveCachelineGCC( typename VECTYPE::PhotonReflections<FLOAT_TYPE, DIM> & dataV )
{
#ifdef DEBUG_RICH
    std::cout << "Solving (new) Quartic Equation for "
            << typeid(VECT).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  auto end = dataV.end();
  //default(none) shared(dataV)
#pragma omp parallel for schedule(static, chunkSize)
  for (auto it = dataV.begin(); it < end; ++it) {
    solveCachelineGCC(*it);
  }
  // for ( const auto& data : dataV ) { solveNew<TYPE>(data); }
}


template<class T,  void (*F) (T&) >
static void benchmark(T& dataV,const std::string funcname )
{
    const auto start = std::chrono::high_resolution_clock::now();
    for(uint64_t i=0;i<nIterations;i++)
        F(dataV);
    const auto end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::nano> diff = end - start;
    auto avg = diff.count()/static_cast<double>(nPhotons*nIterations);
    std::cout << funcname << ", " << avg << std::endl;
}

int main ( int argc, char** argv)
{
  //const unsigned int nPhotons = 1e6;
  //const unsigned int nPhotons = 1e3;
  int ret = 0;

  if (argc > 1) {
      //FIXME: 64 bit? or default int?
    nPhotons = atol(argv[1]);
  }

  if (argc > 2) {
    nIterations = atol(argv[2]);
  }

  if (argc > 3) {
    chunkSize = atoi(argv[3]);
  }

  //Create data generator which produces our test data
#ifdef SKALAR
  RandomDataGenerator<float,16> rndg(nPhotons, true);
  auto dataV0 = &(rndg.m_AOSData);
#else
  RandomDataGenerator<float,16> rndg(nPhotons, false);
#endif
  rndg.generateData();
  
  
  // run the solver for floats
 /* benchmark (solveNew<float>, dataV2, "solveNew<float>" );

  // run the solver for doublesq
  benchmark(solveNew<double>, dataV3, "solveNew<double>" );
*/
  // run the solver for floats (vectorclass)

  auto dataV0_vect = &rndg.m_VecData;
  auto dataV1_vect = &rndg.m_VecData;

  // run the solver for floats
  //shuffle<double,8>(dataV0, dataV1_vect);
  std::cout << "Running Benchmark for " << nPhotons << " photons for " << nIterations << " iterations, chunkSize=" << chunkSize << std::endl;
#ifdef VTUNE
  __itt_resume();
#endif //VTUNE
  benchmark<VECTYPE::PhotonReflections<float>&, solveCacheline>(*dataV0_vect, std::string("solveCacheline<Vec16f>"));
  //benchmark<VECTYPE::PhotonReflections<float>&, solveCacheline<Vec16f, float>>(*dataV0_vect, std::string("solveCacheline<Vec16f>"));
  // benchmark<typeof(dataV0_vect), decltype<solveCacheline>> (solveCacheline, dataV0_vect, "solveCachelineline<float>" );
#ifdef VTUNE
  __itt_pause();
#endif //VTUNE
  // benchmark<typeof(dataV0_vect), decltype<solveCacheline>> (solveCacheline, dataV0_vect, "solveCachelineline<float>" );

//  benchmark<VECTYPE::PhotonReflections<float>&, solveCachelineGCC<Vec16f, float>>(*dataV1_vect, std::string("solveCachelineGCC<float>"));

#ifdef SKALAR
  //run the classic solver
  //benchmark<PhotonData::Vector&, solveClassic>(*dataV0, std::string("solveClassic<VECTOR_TYPE>"));
  benchmark<PhotonData::Vector&, solveClassicParallel>(*dataV0, std::string("solveClassicParallel<VECTOR_TYPE>"));
  benchmark<PhotonData::Vector&, solveClassicNewtonParallel>(*dataV0, std::string("solveClassicNewtonParallel<VECTOR_TYPE>"));
#ifdef DEBUG_RICH
  std::cout << "First reflection point: " << dataV0[0].sphReflPoint << std::endl;
#endif
  // run the solver for doubles
//  benchmark<Data::Vector&, solve<double>>(dataV1, std::string("solve<double>"));
#ifdef DEBUG_RICH
//  std::cout << "First reflection point: " << dataV1[0].sphReflPoint << std::endl;
#endif
#endif /* SKALAR */  
  
#if defined(VALIDATE_DATA) && defined(SKALAR)
  rndg.validateAllData();
#endif
  
#ifdef DEBUG_RICH
  std::cout << "First reflection point Vec16f: \n" << dataV0_vect[0].sphReflPoint << std::endl;
  std::cout << "First reflection point GCCauto_vec: \n" << dataV1_vect[0].sphReflPoint << std::endl;
#endif
#ifdef TIMERS
  std::cout << "Timers (" << nPhotons << "):" << std::endl;
  std::cout << "transform: " << timer_transform.count()/1.0e9 << std::endl;
  std::cout << "quartic: " << timer_quartic.count()/1.0e9 << std::endl;
  std::cout << "cbrt: " << timer_cbrt.count()/1.0e9 << std::endl;
#endif /* TIMERS */

  /*
  benchmark<VECTYPE::PhotonReflections<double,8>&, solveCacheline<Vec8d, double, 8>>(dataV1_vect, std::string("solveCacheline<Vec8d>"));
  std::cout << "1. reflection point: " << dataV1_vect[1].sphReflPoint << std::endl;
  std::cout << "9. reflection point: " << dataV1_vect[7].sphReflPoint << std::endl;
*/
  // run the solver for doubles (vectorclass)
  // FIXME: implement double version
  // benchmark(solveCachelineline<Vec8d>, dataV5, "solveCachelineline<double>" );
  // std::cout << "First reflection point: " << dataV3[0].sphReflPoint << std::endl;

  return ret;
}
