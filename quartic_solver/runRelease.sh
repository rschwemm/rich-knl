#!/bin/bash

CHUNK_SIZE=1000
N_PHOTONS=10000000
N_ITERS=1000

OPTIND=1

while getopts "h?c:p:i:" opt; do
    case "$opt" in
    h|\?)
        echo "Usage: [-c <chunk size>] [-i <number of iterations>] [-p <number of photons>]"
        exit 0
        ;;
    c)  CHUNK_SIZE=$OPTARG
        ;;
    p)  N_PHOTONS=$OPTARG
        ;;
    i)  N_ITERS=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

. ./setupEnv.sh
#numactl --cpunodebind=0 --membind=0 perf stat -e cache-references,cache-misses,cycles,instructions,branches,faults,branch-misses ./release 10000000 1000
#numactl --membind=4,5,6,7 ./release $N_PHOTONS $N_ITERS $CHUNK_SIZE
#KNL
#numactl -C0-63 --membind=4 ./release_inv_sqrt $((2*1048576)) 50000 512
#numactl -C0-63 --membind=4 ./release-no-sin $((2*1048576)) 50000 512
#Broadwell
numactl --cpunodebind=0 --membind=0 ./release 16384000 1000 1024
#numactl --cpunodebind=0 --membind=0 ./release 16384000 1 1024
#numactl -C 0-9 --membind=0 ./release 10000000 1000
