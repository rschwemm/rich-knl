/* 
 * File:   randomDataGenerator.cpp
 * Author: rainer
 * 
 * Created on February 1, 2017, 4:45 PM
 */

#include "randomDataGenerator.h"

void PhotonData::printPhotonData()
{
    std::cout << "emission point       : (" << emissPnt.x() << ", " << emissPnt.y() << ", " << emissPnt.z() << ")" << std::endl;
    std::cout << "center of curvature  : (" << centOfCurv.x() << ", " << centOfCurv.y() << ", " << centOfCurv.z() << ")" << std::endl;
    std::cout << "virt. detection point: (" << virtDetPoint.x() << ", " << virtDetPoint.y() << ", " << virtDetPoint.z() << ")" << std::endl;
    std::cout << "reflection point     : (" << sphReflPoint.x() << ", " << sphReflPoint.y() << ", " << sphReflPoint.z() << ")" << std::endl;
    std::cout << "radius               : " << radius << std::endl;
}


template< class FLOAT_TYPE, std::size_t BATCH_SIZE>
RandomDataGenerator<FLOAT_TYPE, BATCH_SIZE>::RandomDataGenerator(uint64_t nPhotons, bool useAOSData, bool useHalfSOAData, bool useFullSOAData):
m_useAOSData(useAOSData), m_useHalfSOAData(useHalfSOAData), m_useFullSOAData(useFullSOAData), m_nPhotons(nPhotons)
{
    m_nPhotons = ((m_nPhotons/BATCH_SIZE) + 1) * BATCH_SIZE;
    
    if(m_useAOSData)
        m_AOSData.resize(m_nPhotons);
    
    if(m_useHalfSOAData)
        m_VecData.resize(m_nPhotons/BATCH_SIZE);
    
    if(m_useFullSOAData)
        m_FullSOADAta.allocateSpace(m_nPhotons);
    
    m_errorHisto.resize(60);
}

template< class FLOAT_TYPE, std::size_t BATCH_SIZE>
PhotonData* RandomDataGenerator<FLOAT_TYPE, BATCH_SIZE>::generatePhoton(uint64_t seed) 
  {
    // Generate a new rnd instance here to facilitate thread safety
    static std::default_random_engine gen(seed);
    // TODO: time als seed? gen.seed(4);
    // Distributions for each member
    static std::uniform_real_distribution<double> r_emiss_x(-800,800), r_emiss_y(-600,600), r_emiss_z(10000,10500);
    static std::uniform_real_distribution<double> r_coc_x(-3000,3000), r_coc_y(-20,20),     r_coc_z(3300,3400);
    static std::uniform_real_distribution<double> r_vdp_x(-3000,3000), r_vdp_y(-200,200),   r_vdp_z(8100,8200);
    static std::uniform_real_distribution<float>  r_rad(8500,8600);
    // setup data
    PhotonData* photonData = new PhotonData();
    photonData->emissPnt     = Gaudi::XYZPoint( r_emiss_x(gen), r_emiss_y(gen), r_emiss_z(gen) );
    photonData->centOfCurv   = Gaudi::XYZPoint( r_coc_x(gen),   r_coc_y(gen),   r_coc_z(gen)   );
    photonData->virtDetPoint = Gaudi::XYZPoint( r_vdp_x(gen),   r_vdp_y(gen),   r_vdp_z(gen)   );
    photonData->radius       = r_rad(gen);
    photonData->sphReflPoint = Gaudi::XYZPoint( 0.0,   0.0,  0.0   );
    return photonData;
  }

template< class FLOAT_TYPE, std::size_t BATCH_SIZE>
void RandomDataGenerator<FLOAT_TYPE, BATCH_SIZE>::releasePhoton(PhotonData* photonData)
{
    //For now we just free the photon object. It might be good to
    //use a per thread object that is just recycled instead of allocate/deallocate
    delete photonData;
}

template< class FLOAT_TYPE, std::size_t BATCH_SIZE>
void RandomDataGenerator<FLOAT_TYPE, BATCH_SIZE>::generateData()
{
    
    #pragma omp parallel for
    for ( uint64_t i = 0 ; i < m_nPhotons; i++) 
    {
        PhotonData* photonData = generatePhoton(i);
        if(m_useAOSData)
        {
            //Copy the generated photon into the AOS array
            m_AOSData[i] = *photonData;
        }
        
        //convert data from AOS to SOA and insert it into the SOA field
        if(m_useHalfSOAData)
            shuffleAOS(photonData, i, m_VecData);
        
        if(m_useFullSOAData)
            shuffleFullAOS(photonData, i, m_FullSOADAta);
        
        //Notification that we are done with processing this photonData item
        releasePhoton(photonData);
    }    
}


template< class FLOAT_TYPE, std::size_t BATCH_SIZE>
void RandomDataGenerator<FLOAT_TYPE, BATCH_SIZE>::shuffleAOS(const PhotonData* photonData, uint64_t offset, VECTYPE::PhotonReflections<FLOAT_TYPE, BATCH_SIZE>& targetData) {
    // std::cout << len << std::endl;
    // FIXME: Unterschied ob ich lesend oder schreibend springe?
    // FIXME: auto& ?
    auto permpart = [&] (auto &targetField, int itemID, const auto& dataField) {
        targetField.x()[itemID] = dataField.x();
        targetField.y()[itemID] = dataField.y();
        targetField.z()[itemID] = dataField.z();
    };
    auto perm = [&] (auto targetBatch , const PhotonData* photonData, const uint64_t itemID) {
        permpart(targetBatch->emissPnt,     itemID, photonData->emissPnt);
        permpart(targetBatch->centOfCurv,   itemID, photonData->centOfCurv);
        permpart(targetBatch->virtDetPoint, itemID, photonData->virtDetPoint);
                 targetBatch->radius[itemID] = photonData->radius;
    };
    
    const uint64_t batchID = offset / BATCH_SIZE;
    const uint64_t itemID  = offset % BATCH_SIZE;
    
    auto targetBatch = &(targetData[batchID]);
    perm(targetBatch, photonData, itemID);
}

template< class FLOAT_TYPE, std::size_t BATCH_SIZE>
void RandomDataGenerator<FLOAT_TYPE, BATCH_SIZE>::shuffleFullAOS(const PhotonData* photonData, uint64_t offset, VECTYPE::PhotonReflectionArrays<FLOAT_TYPE, BATCH_SIZE>& targetData) {
  
    auto permpart = [&] (auto &targetField, int itemID, const auto& dataField) {
        targetField.x()[itemID] = dataField.x();
        targetField.y()[itemID] = dataField.y();
        targetField.z()[itemID] = dataField.z();
    };
    auto perm = [&] (auto& targetData , const PhotonData* photonData, const uint64_t itemID) {
        permpart(targetData.emissPnt,     itemID, photonData->emissPnt);
        permpart(targetData.centOfCurv,   itemID, photonData->centOfCurv);
        permpart(targetData.virtDetPoint, itemID, photonData->virtDetPoint);
                 targetData.radius[itemID] = photonData->radius;
    };
 
    perm(targetData, photonData, offset);
}


template< class FLOAT_TYPE, std::size_t BATCH_SIZE>
bool RandomDataGenerator<FLOAT_TYPE, BATCH_SIZE>::compareData(const PhotonData& photonData, const VECTYPE::PhotonReflection<FLOAT_TYPE, BATCH_SIZE>& vecData, const size_t itemID)
{
    //Fill in error histogram
    double diff = std::sqrt( ((double)photonData.sphReflPoint.x() - (double)vecData.sphReflPoint.x()[itemID]) * ((double)photonData.sphReflPoint.x() - (double)vecData.sphReflPoint.x()[itemID]) +
                      ((double)photonData.sphReflPoint.y() - (double)vecData.sphReflPoint.y()[itemID]) * ((double)photonData.sphReflPoint.y() - (double)vecData.sphReflPoint.y()[itemID]) +
                      ((double)photonData.sphReflPoint.z() - (double)vecData.sphReflPoint.z()[itemID]) * ((double)photonData.sphReflPoint.z() - (double)vecData.sphReflPoint.z()[itemID]));
    //std::cout << diff << ": ";
    diff = log2(diff);
    //std::cout << diff << std::endl;
    int binID = ((int)diff)+25;
    if(binID < 0)
        binID = 0;
    if(binID > 59)
    {
        binID = 59;
        std::cout << "Bin ID overflow: " << binID << std::endl;
    }
    
    m_errorHisto[binID]++;
    
    //This is explicitly written out 
    if((double)vecData.emissPnt.x()[itemID] != photonData.emissPnt.x() || 
       (double)vecData.emissPnt.y()[itemID] != photonData.emissPnt.y() || 
       (double)vecData.emissPnt.z()[itemID] != photonData.emissPnt.z())
        return false;
    if((double)vecData.centOfCurv.x()[itemID] != photonData.centOfCurv.x() || 
       (double)vecData.centOfCurv.y()[itemID] != photonData.centOfCurv.y() || 
       (double)vecData.centOfCurv.z()[itemID] != photonData.centOfCurv.z())
        return false;
    if((double)vecData.virtDetPoint.x()[itemID] != photonData.virtDetPoint.x() || 
       (double)vecData.virtDetPoint.y()[itemID] != photonData.virtDetPoint.y() || 
       (double)vecData.virtDetPoint.z()[itemID] != photonData.virtDetPoint.z())
        return false;
    if((double)vecData.sphReflPoint.x()[itemID] != photonData.sphReflPoint.x() || 
       (double)vecData.sphReflPoint.y()[itemID] != photonData.sphReflPoint.y() || 
       (double)vecData.sphReflPoint.z()[itemID] != photonData.sphReflPoint.z())
        return false;
    if((double)vecData.radius[itemID] != photonData.radius)
        return false;
    return true;
}

template< class FLOAT_TYPE, std::size_t BATCH_SIZE>
bool RandomDataGenerator<FLOAT_TYPE, BATCH_SIZE>::validateAllData()
{
    auto soa = &(m_VecData);
    auto aos = &m_AOSData;

    auto soaItem = (*soa)[0];
    auto aosItem = (*aos)[0];
    
    uint64_t batchID = 0;
    uint64_t itemID = 0;    
    
    bool ret = true;
    bool first = true;
    for(uint64_t i = 0; i < m_nPhotons; i++)
    {
        
        batchID = i / BATCH_SIZE;
        itemID  = i % BATCH_SIZE;
        
        soaItem = (*soa)[batchID];
        aosItem = (*aos)[i];
  
       
        if(!compareData(aosItem, soaItem, itemID))
        {
            if (first)
            {
              std::cerr << "Error while comparing aos and soa data." << std::endl;
              std::cerr << "AOS data" << std::endl;
              aosItem.printPhotonData();
              std::cerr << "SOA data" << std::endl;
              soaItem.printPhotonData(itemID);
              first = false;
            }
            ret = false;
        }
    }
    int count = 0; 
    uint64_t photons = 0;
    uint64_t prevPhotons = 0;
    double bin = 0;
    std::cout << "Reflection point difference" << std::endl;
    for(auto it = m_errorHisto.begin(); it < m_errorHisto.end(); it++)
    {
        bin = std::pow(2, (count - 25));
        photons += *it;
        std::cout << std::scientific << bin << ": " << *it << std::endl;
        
        if (photons > (0.6827 * m_nPhotons) && (prevPhotons < 0.6827 * m_nPhotons))
            std::cout << "1 x sigma" << std::endl;
        if (photons > (0.9545 * m_nPhotons) && (prevPhotons < 0.9545 * m_nPhotons))
            std::cout << "2 x sigma" << std::endl;
        if (photons > (0.9973 * m_nPhotons) && (prevPhotons < 0.9973 * m_nPhotons))
            std::cout << "3 x sigma" << std::endl;            
       if (photons > (0.999936 * m_nPhotons) && (prevPhotons < 0.999936 * m_nPhotons))
            std::cout << "4 x sigma" << std::endl;                    
        prevPhotons = photons;
        
        count += 1;
    }
    return ret;
}

#ifdef TEST_DATA_GEN
//Small test to validate the consistency of the shuffled data
int main(int argc, char** argv)
{
    size_t batchSize = 16;
    uint64_t nTestPhotons=1000000 * batchSize;
    std::cout << "Allocating output arrays..." << std::endl << std::flush;
    RandomDataGenerator<float> rndg(nTestPhotons, true);
    std::cout << "Done" << std::endl << std::flush;
    
    std::cout << "Generating data..." << std::endl << std::flush;
    rndg.generateData();
    std::cout << "Done" << std::endl << std::flush;
    
    auto soa = &(rndg.m_VecData);
    auto aos = &rndg.m_AOSData;
    uint64_t batchID = 0;
    uint64_t itemID = 0;
    
    auto soaItem = (*soa)[0];
    auto aosItem = (*aos)[0];
    
    std::cout << "Comparing by-construction unequal items..." << std::endl << std::flush;
    //Make sure to trigger an error to check if error detection works    
    if(rndg.compareData(aosItem, soaItem, 1))
        std::cout << "compare function claims that soaItem[1] == aosItem[0]. This might be a bug in the compare function." << std::endl;
    std::cout << "Done" << std::endl << std::flush;
    
    std::cout << "Comparing all items..." << std::endl << std::flush;
    for(uint64_t i = 0; i < nTestPhotons; i++)
    {
        batchID = i / batchSize;
        itemID  = i % batchSize;
        
        soaItem = (*soa)[batchID];
        aosItem = (*aos)[i];
  
        if(!rndg.compareData(aosItem, soaItem, itemID))
        {
            std::cerr << "Error while comparing aos and soa data." << std::endl;
            std::cerr << "AOS data" << std::endl;
            aosItem.printPhotonData();
            std::cerr << "SOA data" << std::endl;
            soaItem.printPhotonData(itemID);
        }
    }    
    aosItem = (*aos)[nTestPhotons-1];
    soaItem = (*soa)[(nTestPhotons/batchSize)-1];
        
    std::cout << "Done" << std::endl << std::flush;    
    std::cout << "AOS data at end of array" << std::endl;
    aosItem.printPhotonData();
    std::cout << "SOA data at end of array" << std::endl;
    soaItem.printPhotonData(batchSize-1);
}
#endif