#ifndef INTRINSIC_CBRT
#define INTRINSIC_CBRT
#endif

#include "VectorClass/vectorclass.h"
#include "immintrin.h"


static inline Vec16f intrinsic_crbt16(const Vec16f& v)
{
  const Vec8f l = v.get_low();
  const Vec8f u = v.get_high();
//  const __m256* l = (__m256*)v;
//  const __m256* u = (__m256*)(v + 8);

  return Vec16f(_mm256_cbrt_ps(l), _mm256_cbrt_ps(u));
}
