#!/bin/bash

unset LD_LIBRARY_PATH
. /sw/intel/xe2017/all-setup.sh
. /cvmfs/lhcb.cern.ch/lib/lcg/releases/gcc/6.2.0/x86_64-slc6/setup.sh
#. /cvmfs/lhcb.cern.ch/lib/lcg/releases/gcc/4.9.3/x86_64-slc6/setup.sh
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/ROOT/6.04.02/x86_64-slc6-gcc48-opt/lib
