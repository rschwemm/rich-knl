#include "gtest/gtest.h"

#include "vectorclass512.h"
#include "cbrt.h"
#include <tuple>

#define MAXDIFF 0.00001

TEST(CbrtTest, FloatSmallNum) {
    std::tuple<float, float> res(10, 3);
    EXPECT_EQ(res, MyRoots::cbrt(1000.0f,27.0f));
}

TEST(CbrtTest, DoubleSmallNum) {
    std::tuple<double, double> res(10, 3);
    EXPECT_EQ(res, MyRoots::cbrt(1000.0f,27.0f));
}

TEST(CbrtTest, Vec16fSmallNum) {
    Vec16f vec{0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0};
    Vec16f shouldbe{ 5.51301e-14, 1, 1.25992, 1.44225, 1.5874, 1.70998, 1.81712, 1.91293, 2, 2.08008, 2.15443, 2.22398, 2.28943, 2.35133, 2.41014, 2.46621};
    auto res =  MyRoots::cbrt(vec);
    constexpr auto vecsize = 16;
    for(int i = 0; i < vecsize; i++) {
        EXPECT_NEAR(shouldbe[i], res[i], shouldbe[i]*MAXDIFF);
    }
}

TEST(CbrtTest, Vec16fBigNum) {
    Vec16f vec{1.03535e+06, 1.03535e+06, 4.4774e+06, 4.4774e+06, 1.93627e+07, 1.93627e+07, 8.37348e+07, 8.37348e+07, 3.62115e+08, 3.62115e+08, 1.56598e+09, 1.56598e+09, 6.77215e+09, 6.77215e+09, 2.92865e+10, 2.92865e+10};
    Vec16f shouldbe{101.165, 101.165, 164.819, 164.819, 268.527, 268.527, 437.491, 437.491, 712.769, 712.769, 1161.26, 1161.26, 1891.95, 1891.95, 3082.4, 3082.4};
    auto res =  MyRoots::cbrt(vec);
    constexpr auto vecsize = 16;
    for(int i = 0; i < vecsize; i++) {
        EXPECT_NEAR(shouldbe[i], res[i], shouldbe[i]*MAXDIFF);
    }
}

