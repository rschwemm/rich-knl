import re

evec = []
dvec = []
CoC = []
sinbeta = []
radius = []
e = []
sphReflPoint = []

FILE="transform_vals.dump"

def str2point(vec):
    return vec.split(': ')[1].split('\n')[0];

def str2vec(vec):
    return vec.split('(')[1].split(')')[0].split(',')

def point2str(first, vec):
    return first + '{'+''.join('%s, ' % sublist for sublist in vec[:16])+'};'

def vec2str(first, vec, i):
    return first + '{'+''.join('%s, ' % sublist[i] for sublist in vec[:16])+'};'

f = open(FILE, 'r')
for line in f:
    if "evec" in line:
        evec.append(str2vec(line))
    elif "dvec" in line:
        dvec.append(str2vec(line))
    elif "CoC" in line:
        CoC.append(str2vec(line))
    elif "sinbeta" in line:
        sinbeta.append(str2point(line))
    elif "radius" in line:
        radius.append(str2point(line))
    elif "e:" in line:
        e.append(str2point(line))
    elif "sphReflPoint" in line:
        sphReflPoint.append(str2vec(line))
    else:
        print("undefined!");


print("****")
print(vec2str("Vec16f evecX", evec, 0))
print("****")
print(vec2str("Vec16f evecY", evec, 1))
print("****")
print(vec2str("Vec16f evecZ", evec, 2))
print("****")
print(vec2str("Vec16f dvecX", dvec, 0))
print("****")
print(vec2str("Vec16f dvecY", dvec, 1))
print("****")
print(vec2str("Vec16f dvecZ", dvec, 2))
print("****")
print(vec2str("Vec16f CoCX", CoC, 0))
print("****")
print(vec2str("Vec16f CoCY", CoC, 1))
print("****")
print(vec2str("Vec16f CoCZ", CoC, 2))
print("****")
print(point2str("Vec16f sinbeta", sinbeta))
print("****")
print(point2str("Vec16f radius", radius))
print("****")
print(point2str("Vec16f e", e))
print("****")
print(vec2str("Vec16f sphReflPointX", sphReflPoint, 0))
print("****")
print(vec2str("Vec16f sphReflPointY", sphReflPoint, 1))
print("****")
print(vec2str("Vec16f sphReflPointZ", sphReflPoint, 2))
print("****")
