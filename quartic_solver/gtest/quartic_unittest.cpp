#include "gtest/gtest.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#include "QuarticSolverCacheline.h"
#include "QuarticSolverNew.h"
#include "QuarticSolver.h"
#include "vectype.h"

#pragma GCC diagnostic pop

#define MAXDIFF 0.00001

TEST(SolveQuarticTest, Vec16fQuarticSolv) {
    Vec16f veca{0.292734, 0.345842, 0.580669, 0.167268, 0.279434, 0.37221, 0.188104, 0.12402,
                0.217786, 0.352972, 0.266886, 0.298822, 0.53575, 0.07253, 0.254614, 0.149041};
    Vec16f vecb{0.773326, 1.21709, 0.741726, 1.1292, 1.05418, 1.25813, 1.23826, 1.25938,
                1.13079, 1.21874, 0.967601, 1.23473, 1.06343, 1.13082, 1.23899, 1.0719};
    Vec16f vecc{0.0325617, 0.0562948, 0.0777241, 0.0232759, 0.0427928, 0.0522997, 0.0308479, 0.0181859,
                0.0356312, 0.0584653, 0.0314267, 0.0449198, 0.0920127, 0.0117386, 0.0419711, 0.0250355};
    Vec16f vecd{-0.00385396, -0.00462552, -0.0157168, -0.00124031, -0.00281139, -0.00733585, -0.00127236, -0.00068969,
                -0.00154945, -0.00466222, -0.00362797, -0.00399606, -0.00999216, -0.000168878, -0.00236721, -0.000609412};

    Vec16f shouldbe{0.0529255, 0.0428651, 0.104675, 0.0244277, 0.0352823, 0.0586703, 0.0219548, 0.017279,
                    0.0245063, 0.0424996, 0.0472994, 0.0416624, 0.06342, 0.00808792, 0.0299918, 0.0148799};


    RichCacheline::Rec::QuarticSolverCacheline qSolverCacheline;
    Rich::Rec::QuarticSolver qSolver;
    auto sinbeta = qSolverCacheline.solve_quartic_RICH<Vec16f, float>(veca, vecb, vecc, vecd);
    constexpr auto vecsize = 16;

    for(int i = 0; i < vecsize; i++) {
        auto tmp = qSolver.solve_quartic_RICH<float>(veca[i], vecb[i], vecc[i], vecd[i]);
        EXPECT_NEAR(tmp, sinbeta[i], abs(tmp*MAXDIFF));
    }
}

TEST(SolveQuarticTest, Vec16fTransform) {
    Vec16f evecX{-566.164, -1908.06, 786.901, -2660.52, -1951.89, -1754.75, -1850.23, -2760.33, 2022.5, -2248.78, -1273.65, -1725.36, -665.245, 2205.79, -987.119, 2208.9};
    Vec16f evecY{-67.0076, 110.132, 563.672, 494.938, 319.064, -262.968, -341.27, -2.55661, 156.066, -508.705, 368.086, -81.6222, 470.181, -606.586, -124.774, 131.868};
    Vec16f evecZ{6697.88, 6899.55, 6875.4, 7055.94, 6656.2, 6877.32, 7093.56, 6833.9, 6719.57, 6667.77, 6889.7, 6908.29, 7059.38, 6983.33, 6779.85, 6669.43};
    Vec16f dvecX{-3070.31, -1666.09, -1619.43, -2508.1, -1272.83, -1486.93, -346.565, -3115.65, 1242.38, -3352.91, -1126.63, -1310.11, -1818.84, 3352.46, 1196.22, 2700.66};
    Vec16f dvecY{-5.50764, 97.7928, -105.426, -79.8131, -82.653, 195.995, -69.5559, -101.395, -142.249, 77.856, -97.2614, 75.7003, 124.819, -169.48, -186.835, 89.1532};
    Vec16f dvecZ{4735.57, 4817.05, 4840.5, 4751.41, 4820.01, 4824.72, 4796.53, 4755.17, 4809.86, 4786.85, 4832.17, 4766.26, 4745.04, 4838.97, 4801.29, 4770.81};
    Vec16f CoCX{116.498, 2461.92, -381.532, 2686.59, 1352.47, 1814.43, 1258.92, 2899.83, -1780.5, 1853.67, 1213.24, 2316.19, 87.9567, -1768.43, 1703.19, -1683.54};
    Vec16f CoCY{17.3877, -3.36003, 15.3883, -9.35422, 5.18174, -13.8512, -3.42827, 2.24585, -0.942728, -6.01903, 12.9879, 9.66864, -2.15906, 7.29977, -8.6386, -10.656};
    Vec16f CoCZ{3367.89, 3309.2, 3307.27, 3398.66, 3368.85, 3328.72, 3327.46, 3359.01, 3380.31, 3378.32, 3363.27, 3380.07, 3398.94, 3331.92, 3326.92, 3398.79};
    Vec16f sinbeta{0.161492, 0.0142093, 0.10803, 0.0267059, 0.0166779, 0.0207451, 0.0315546, 0.0476212, 0.0147983, 0.0922159, 0.0196154, 0.00772709, 0.0625911, 0.0785637, 0.0943451, 0.054138};
    Vec16f radius{8538.34, 8575.64, 8548.65, 8546.44, 8559.11, 8589.07, 8515.03, 8596.11, 8513.12, 8586.09, 8510.34, 8568.21, 8599.91, 8562.96, 8524.41, 8563.34};
    Vec16f e{6722.1, 7159.37, 6943.2, 7557.09, 6943.82, 7102.52, 7338.83, 7370.32, 7019.08, 7055.14, 7016.1, 7120.95, 7106.23, 7348.49, 6852.47, 7026.95};
    Vec16f shouldbeX{-1967, 59.4445, -312.11, -503.926, -1000.39, -402.398, -631.087, -693.971, 598.771, -1596.62, -421.43, 197.712, -1246.31, 1429.21, 1275.46, 1444.48};
    Vec16f shouldbeY{-43.7518, 135.925, 505.382, 427.476, 267.559, -182.372, -353.738, -37.3325, 88.9352, -400.721, 319.127, -38.784, 488.169, -585.114, -207.441, 144.673};
    Vec16f shouldbeZ{11647.9, 11540.3, 11841.6, 11315.2, 11594, 11625.1, 11622.7, 11167.7, 11553.7, 11230.7, 11709.5, 11682.1, 11880.6, 11253.3, 11838.3, 11368.9};

    RichCacheline::Rec::QuarticSolverCacheline qSolverCacheline;
    RichNew::Rec::QuarticSolverNew qSolver;
    Vec16f sphReflPointX, sphReflPointY, sphReflPointZ;

    std::tie (sphReflPointX,sphReflPointY, sphReflPointZ)  = qSolverCacheline.transform(evecX, evecY, evecZ, dvecX, dvecY, dvecZ, CoCX, CoCY, CoCZ, sinbeta, radius, e);
    constexpr auto vecsize = 16;

    typedef RichNew::Rec::QuarticSolverNew::Vector Vector;

    for(int i = 0; i < vecsize; i++) {
        const Vector evec(evecX[i], evecY[i], evecZ[i]);
        const Vector dvec(dvecX[i], dvecY[i], dvecZ[i]);
        const Gaudi::XYZPoint CoC(CoCX[i], CoCY[i], CoCZ[i]);

        Gaudi::XYZPoint shouldbe  = qSolver.transform<float>(evec, dvec, CoC,
                                            sinbeta[i], radius[i], e[i]);
        EXPECT_FLOAT_EQ(shouldbe.x(), sphReflPointX[i]);
        EXPECT_FLOAT_EQ(shouldbe.y(), sphReflPointY[i]);
        EXPECT_FLOAT_EQ(shouldbe.z(), sphReflPointZ[i]);
    }
}

TEST(SolveQuarticTest, Vec16fFullQuarticSolv) {
    using  T = float;
    const std::size_t DIM = 16;

    VECTYPE::PhotonReflection<T, DIM> pr;
    pr.emissPnt.x() = {-485.943, 62.415, 225.512, 294.82, -375.447, 106.827, -35.288, 258.941, -235.749, 205.455, -137.137, 14.5232, 553.904, -30.7445, 475.028, 465.437, };
    pr.emissPnt.y() = {-284.033, 284.423, -279.466, 594.516, -181.691, -226.409, 522.676, 586.948, -84.8345, 581.604, -194.901, -22.0516, -143.059, -570.01, -203.309, -309.091, };
    pr.emissPnt.z() = {10262.3, 10267.2, 10172.1, 10093.4, 10051.8, 10153.7, 10182.8, 10102.1, 10194.9, 10435.6, 10125.1, 10111, 10249.3, 10240.3, 10484.5, 10365.6, };
    pr.centOfCurv.x() = {1892.93, -1599.15, -1024.33, 1836.59, -196.176, 1206.24, 1874.54, -1207.12, 219.084, 2854.04, -43.9245, -1720.16, 694.183, 725.994, 2534.03, -1708.52, };
    pr.centOfCurv.y() = {-9.71593, -1.12108, -13.692, -12.8772, -2.36706, -1.75044, -3.98732, -5.67449, -19.5946, 7.20056, 5.49161, -13.9173, 9.31936, 6.59031, 17.1789, 12.4466, };
    pr.centOfCurv.z() = {3351.23, 3386.25, 3305.28, 3343.9, 3391.51, 3306.29, 3353.27, 3378.46, 3372.91, 3389.24, 3357.5, 3363.03, 3355.32, 3340.55, 3330.7, 3349.34, };
    pr.virtDetPoint.x() = {-1060.06, 424.919, 2959.81, -1503.66, 344.973, -1244.87, -681.211, -64.3068, -1601.12, 31.6497, 637.536, -2744.88, 2302.78, -2470.78, -1224.58, -2280.8, };
    pr.virtDetPoint.y() = {-101.037, 166.176, 18.4349, 54.9742, 146.207, -149.939, -65.3358, 35.7236, 185.953, 17.1518, -139.488, 185.495, 151.719, 188.42, -9.88739, -103.121, };
    pr.virtDetPoint.z() = {8145.2, 8102.3, 8106.47, 8102.97, 8132, 8144.69, 8107.63, 8195.64, 8187.03, 8136.91, 8108.54, 8145.96, 8146.85, 8177.47, 8185.05, 8123.23, };
    pr.radius = {8510.35, 8576.92, 8543.37, 8550.67, 8582.68, 8541.1, 8531.41, 8536.6, 8550.99, 8515.61, 8534.42, 8564.61, 8596.29, 8544.2, 8525.03, 8560.41, };

    RichCacheline::Rec::QuarticSolverCacheline qSolverCacheline;
    Rich::Rec::QuarticSolver qSolver;
    qSolverCacheline.solve<Vec16f, float>(pr);
    constexpr auto vecsize = 16;

    for(int i = 0; i < vecsize; i++) {
        const Gaudi::XYZPoint emissPnt = {pr.emissPnt.x()[i], pr.emissPnt.y()[i],
            pr.emissPnt.z()[i]};
        const Gaudi::XYZPoint centOfCurv = {pr.centOfCurv.x()[i], pr.centOfCurv.y()[i],
            pr.centOfCurv.z()[i]};
        const Gaudi::XYZPoint virtDetPoint = {pr.virtDetPoint.x()[i],
            pr.virtDetPoint.y()[i], pr.virtDetPoint.z()[i]};
        Gaudi::XYZPoint sphReflPoint;

        qSolver.solve<double>(emissPnt, centOfCurv, virtDetPoint, pr.radius[i], sphReflPoint);
        // EXPECT_FLOAT_EQ(static_cast<double>(sphReflPoint.x()), pr.sphReflPoint.x()[i]);
        // EXPECT_FLOAT_EQ(static_cast<double>(sphReflPoint.y()), pr.sphReflPoint.y()[i]);
        // EXPECT_FLOAT_EQ(static_cast<double>(sphReflPoint.z()), pr.sphReflPoint.z()[i]);
        EXPECT_NEAR(static_cast<double>(sphReflPoint.x()), pr.sphReflPoint.x()[i], abs(sphReflPoint.x()*MAXDIFF));
         EXPECT_NEAR(static_cast<double>(sphReflPoint.y()), pr.sphReflPoint.y()[i], abs(sphReflPoint.y()*MAXDIFF));
        EXPECT_NEAR(static_cast<double>(sphReflPoint.z()), pr.sphReflPoint.z()[i], abs(sphReflPoint.z()*MAXDIFF));
    }
}
