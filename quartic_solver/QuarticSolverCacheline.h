
//----------------------------------------------------------------------
/** @file QuarticSolverNew.h
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2015-01-27
 */
//----------------------------------------------------------------------

#ifndef RICHRECPHOTONTOOLS_QuarticSolverCacheline_H
#define RICHRECPHOTONTOOLS_QuarticSolverCacheline_H

#if not defined STL
// Gaudi
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#else

// STL
//#include <math.h>
#include <type_traits>
//#include <complex>

#endif

// VectorClass
#include "vectorclass512.h"
#include "VectorClass/complexvec.h"
#include "VectorClass/vectormath_trig.h"
#include "VectorClass/vectormath_exp.h"

#include "vectype.h"
#include "cbrt.h"
#include "richMath.h"
//#include "intriniscCbrt.h"

// Eigen
#include "LHCbMath/EigenTypes.h"
#include <Eigen/Geometry>

// VDT
//#include "vdt/asin.h"

// LHCb Maths
#include "LHCbMath/FastRoots.h"
#include <tuple>

#include "debug.h"

namespace RichCacheline
{
    namespace Rec
    {
        template< class VECT>
        class QuarticSolverConstsVect
        {
        public:
            const VECT c1 = VECT(1.0f);
            const VECT c2 = VECT(2.0f);
            const VECT c4 = VECT(4.0f);
            const VECT c3 = VECT(3.0f);
            const VECT c27 = VECT(27.0f);
            
            const VECT c1_2 = VECT(1.0f) / VECT(2.0f);
            const VECT c1_3 = VECT(1.0f) / VECT(3.0f);
            const VECT c1_4 = VECT(1.0f) / VECT(4.0f);
            const VECT c1_8 = VECT(1.0f) / VECT(8.0f);
            const VECT c1_9 = VECT(1.0f) / VECT(9.0f);
            const VECT c1_54 = VECT(1.0f) / VECT(54.0f);
            
            const VECT c3_8 = VECT(3.0f) / VECT(8.0f);
            const VECT c3_16 = VECT(3.0f) / VECT(16.0f);

            const VECT UU = VECT(-( std::sqrt(3.0) / 2.0 ));
            
            //TODO: This needs to be adjusted to float/double depending on what
            //is chosen as template
            const VECT maxval = VECT(std::numeric_limits<float>::max());
            //const static VECT UU = -0.866025404; // - sqrt(3)/2
        };
        
        template <typename T>
        class QuarticSolverConsts
        {
        public:
            const T c1 = 1.0f;
            const T c2 = 2.0f;
            const T c3 = 3.0f;
            const T c27 = 27.0f;
            
            const T c1_2 = 1.0f / 2.0f;
            const T c1_3 = 1.0f / 3.0f;
            const T c1_4 = 1.0f / 4.0f;
            const T c1_8 = 1.0f / 8.0f;
            const T c1_9 = 1.0f / 9.0f;
            const T c1_54 = 1.0f / 54.0f;
            
            const T c3_8 = 3.0f / 8.0f;
            const T c3_16 = 3.0f / 16.0f;

            const T UU { -( std::sqrt(3.0) / 2.0 ) };
            
            const T maxval = std::numeric_limits<T>::max();
            //const static VECT UU = -0.866025404; // - sqrt(3)/2
        };        
        //-----------------------------------------------------------------------------
        /** @class QuarticSolverCacheline
         *
         *  Utility class that implements the solving of the Quartic equation for the RICH
         *
         *  @author Chris Jones         Christopher.Rob.Jones@cern.ch
         *  @date   2015-01-27
         */
        //-----------------------------------------------------------------------------
        template<class VECT, typename FLOAT_TYPE>
        class QuarticSolverCacheline
        {

            public:

                // Use eigen types
                typedef LHCb::Math::Eigen::XYZPoint  Point;   ///< Point type
                typedef LHCb::Math::Eigen::XYZVector Vector;  ///< vector type
            
            private:
                const QuarticSolverConstsVect<VECT>  __attribute__((__aligned__(64))) m_constants = QuarticSolverConstsVect<VECT>();
            

            public:
 
                /** Solves the characteristic quartic equation for the RICH optical system.
                 *
                 *  See note LHCB/98-040 RICH section 3 for more details
                 *
                 *  @param emissionPoint Assumed photon emission point on track
                 *  @param CoC           Spherical mirror centre of curvature
                 *  @param virtDetPoint  Virtual detection point
                 *  @param radius        Spherical mirror radius of curvature
                 *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
                 *
                 *  @return boolean indicating status of the quartic solution
                 *  @retval true  Calculation was successful. sphReflPoint is valid.
                 *  @retval false Calculation failed. sphReflPoint is not valid.
                 */
                //template< class VECTYPE, class FLOAT_TYPE >
                void solve( VECTYPE::PhotonReflections<FLOAT_TYPE>& dataV, uint64_t chunksize) const
                {
                    auto end = dataV.end();
                    //float sinBetaHisto[2000] = {0.0};
                    //float sinBetaMinMax[2];
                    //float a1minMax[2];
                    //float a2minMax[2];
                    //float a3minMax[2];
                    //float a4minMax[2];
                    #pragma omp parallel for schedule(static, chunksize)
                    for (auto it = dataV.begin(); it < end; ++it) 
                    {                    
                        //Prefetch radius for current loop (it comes after quite a few calculations)
                        __builtin_prefetch(&((it+0)->radius[0]), 0, 3);

                        //Prefetch data for next looop
                        //Tried to prefetch only some of these, but best performance on Broadwell is with
                        //just prefetching all of them
                        __builtin_prefetch(&((it+1)->emissPnt.x()[0]), 0, 3);
                        __builtin_prefetch(&((it+1)->emissPnt.y()[0]), 0, 3);
                        __builtin_prefetch(&((it+1)->emissPnt.z()[0]), 0, 3);
                        __builtin_prefetch(&((it+1)->centOfCurv.x()[0]), 0, 3);
                        __builtin_prefetch(&((it+1)->centOfCurv.y()[0]), 0, 3);
                        __builtin_prefetch(&((it+1)->centOfCurv.z()[0]), 0, 3);
                        __builtin_prefetch(&((it+1)->virtDetPoint.x()[0]), 0, 3);
                        __builtin_prefetch(&((it+1)->virtDetPoint.y()[0]), 0, 3);
                        __builtin_prefetch(&((it+1)->virtDetPoint.z()[0]), 0, 3);

                        for(int i = 0; i < 16; i+=16)
                        {
                            VECT emissionPointVecX;
                            emissionPointVecX.load_a(&(it->emissPnt.x())[i]);
                            VECT emissionPointVecY;
                            emissionPointVecY.load_a(&(it->emissPnt.y())[i]);
                            VECT emissionPointVecZ;
                            emissionPointVecZ.load_a(&(it->emissPnt.z())[i]);

                            VECT CoCX;
                            CoCX.load_a(&(it->centOfCurv.x())[i]);
                            VECT CoCY;
                            CoCY.load_a(&(it->centOfCurv.y())[i]);
                            VECT CoCZ;
                            CoCZ.load_a(&(it->centOfCurv.z())[i]);


                            //const Vector evec( emissionPoint - CoC );
                            const VECT evecX = emissionPointVecX - CoCX;
                            const VECT evecY = emissionPointVecY - CoCY;
                            const VECT evecZ = emissionPointVecZ - CoCZ;

                            // vector from mirror centre of curvature to assumed emission point
                            // const TYPE e2 = evec.dot(evec);
                            const VECT e2 = evecX*evecX + evecY*evecY + evecZ*evecZ;

                            // vector from mirror centre of curvature to virtual detection point
                            VECT virtDetPointVecX;
                            virtDetPointVecX.load_a(&(it->virtDetPoint.x())[i]);
                            VECT virtDetPointVecY;
                            virtDetPointVecY.load_a(&(it->virtDetPoint.y())[i]);
                            VECT virtDetPointVecZ;
                            virtDetPointVecZ.load_a(&(it->virtDetPoint.z())[i]);

                            // const Vector dvec( virtDetPoint - CoC );
                            const VECT dvecX = virtDetPointVecX - CoCX;
                            const VECT dvecY = virtDetPointVecY - CoCY;
                            const VECT dvecZ = virtDetPointVecZ - CoCZ;

                            // const TYPE d2 = dvec.dot(dvec);
                            const VECT d2 = dvecX*dvecX + dvecY*dvecY + dvecZ*dvecZ;

                            // various quantities needed to create quartic equation
                            // see LHCB/98-040 section 3, equation 3
                            //const auto ed2 = e2 * d2;
                            const VECT ed2 = e2 * d2;
                            const VECT evecDvec = evecX*dvecX + evecY*dvecY + evecZ*dvecZ;


                            // TODO: compare at this point
                            // approx_recipr(a); 
                            VECT cosgamma2 = RichMath::vectorDiv((evecDvec * evecDvec),ed2);
                            cosgamma2 = select(ed2 > 0, cosgamma2, m_constants.c1);

                            const VECT e = RichMath::vectorSqrt<VECT>(e2);
                            const VECT d1 = RichMath::vectorSqrt<VECT>(d2);

                            const VECT singamma = RichMath::vectorSqrt<VECT>(m_constants.c1 - cosgamma2);  // TODO: kann es echt > 1.0 werden?
                            const VECT cosgamma = RichMath::vectorSqrt<VECT>(cosgamma2);

                            // const auto dx        = d * cosgamma;
                            // const auto dy        = d * singamma;
                            // const auto r2        = radius * radius;
                            // const auto dy2       = dy * dy;
                            // const auto edx       = e + dx;
                            const VECT dx = d1 * cosgamma;
                            const VECT dy = d1 * singamma;

                            VECT radius;
                            radius.load_a(&(it->radius)[i]);

                            const VECT r2 = radius * radius;
                            const VECT dy2 = dy * dy;
                            const VECT edx = e + dx;

                            // Fill array for quartic equation
                            // const auto a0      =     4.0 * ed2;
                            // const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 : std::numeric_limits<TYPE>::max() );
                            // const auto dyrad2  =     2.0 * dy * radius;
                            // const auto a1      = - ( 2.0 * dyrad2 * e2 ) * inv_a0;
                            // const auto a2      =   ( (dy2 * r2) + ( edx * edx * r2 ) - a0 ) * inv_a0;
                            // const auto a3      =   ( dyrad2 * e * (e-dx) ) * inv_a0;
                            // const auto a4      =   ( ( e2 - r2 ) * dy2 ) * inv_a0;

                            const VECT a0 = m_constants.c4 * ed2;
                            // FIXME: warum floatmax, statt inf?
                            const VECT maxval  =  m_constants.maxval; //std::numeric_limits<FLOAT_TYPE>::max();
                            //TODO: THis might be a problem on non AVX512fX platforms
                            //const VECT inv_a0    = select(a0 > 0, RichMath::vectorDiv<VECT>(1.0, a0), maxval);
                            const VECT dyrad2    = m_constants.c2 * dy *radius;
                            const VECT a        = -( m_constants.c2 * dyrad2 * e2 );// * inv_a0;
                            const VECT b        = ( ( dy2* r2 ) + (edx * edx * r2) - a0 );// * inv_a0;
                            const VECT c        = (dyrad2 * e * (e-dx));// * inv_a0;
                            const VECT d        = ( (e2 - r2) * dy2 );// * inv_a0;

                            const auto sinbeta = newtonRoot(a0,a,b,c,d);
                            // use simplified RICH version of quartic solver
                            TIMER_START(timer_quartic);
                            //const auto sinbeta = solve_quartic_RICH( a1, a2, a3, a4);
/*
                                    const VECT r4 = m_constants.c1_4;
                                    const VECT q2 = m_constants.c1_2;
                                    const VECT q8 = m_constants.c1_8;
                                    const VECT q1 = m_constants.c3_8;
                                    const VECT q3 = m_constants.c3_16;


                                    const auto aa = a * a;
                                    const auto pp = b - q1 * aa;
                                    const auto qq = c - (q2 * a * (b - (r4 * aa)));
                                    const auto rr = d - r4 * (a * c - r4 * aa * (b - q3 * aa));
                                    const auto rc = q2 * pp;
                                    const auto sc = r4 * (r4 * pp * pp - rr);
                                    //const auto tc = -pow( q8 * qq, 2 );
                                    const auto tc = -(q8 * q8 * qq * qq);

                                    const auto qcub = (rc * rc - m_constants.c3 * sc);
                                    const auto rcub = (m_constants.c2 * rc * rc * rc - 9 * rc * sc + m_constants.c27 * tc);

                                    const auto Q = qcub * m_constants.c1_9;;
                                    const auto R = rcub * m_constants.c1_54;;

                                    const auto Q3 = Q * Q * Q;
                                    const auto R2 = R * R;

                                    // FIXME: saturated?
                                    // const auto toberooted = (TYPE)( abs_saturated(R) + RichMath::vectorSqrt<VECT>(abs_saturated(R2-Q3)) )
                                    const auto toberooted = (abs(R) + RichMath::vectorSqrt<VECT>(abs(R2-Q3)) );

                                    // FIXME: oder zuerst in normales array, dann load?
                                    // FIXME: also for double?
                                    // FIXME: magic numbers are eval
                                    // FIXME: cbrt with one param of type Vec16f or Vec8d
                                    // FIXME: 16 only for float
                                    TIMER_START(timer_cbrt);
                                    const auto rooted = cbrt(toberooted);
                                    //const auto rooted = intrinsic_crbt16(toberooted) 
                                    TIMER_STOP(timer_cbrt);
                                    const auto sgnR = select( R >= 0, -1, 1 );
                                    const auto A = sgnR * rooted;
                                    //const auto A = sign_combine(rooted, R);
                                    PR0(A);

                                    const auto B = Q / A;

                                    const auto u1 = -q2 * (A + B) - rc * m_constants.c1_3;
                                    // FIXME: saturated or not?
                                    //const auto u2 = UU * abs_saturated(A-B);
                                    const auto u2 = m_constants.UU * abs(A-B);
                                    const auto V = RichMath::vectorSqrt<VECT>(u1*u1 + u2*u2);
                                    // const std::complex<TYPE> w3 = ( abs_satured(V) != 0.0 ? (TYPE)( qq * -0.125 ) / V :
                                    //                                std::complex<TYPE>(0,0) );
                                    // FIXME: warum abs saturated when compared to 0.0 ??
                                    const auto w3r = select(V != 0.0, (qq * -q8)/V, 0.0);
                                    //        const TYPE res = std::real(w1) + std::real(w2) + std::real(w3) - (r4*a);
                                    const auto res = RichMath::vectorSqrt<VECT>((u1+V) * m_constants.c2) + w3r - (r4*a);
                                    // return the final result
                                    // FIXME: std::move ?
                                    const auto sinbeta = select(res >  m_constants.c1,  m_constants.c1, select(res < -m_constants.c1, -m_constants.c1, res));

                                    
                            auto extractMinMax = [&] (auto& minMaxArray, const auto& value)
                            {
                                minMaxArray[0] = value < minMaxArray[0] ? value : minMaxArray[0];
                                minMaxArray[1] = value > minMaxArray[1] ? value : minMaxArray[1];
                            };
                            for(int z = 0; z < 16; z++)
                            {
                                //sinBetaHisto[(int)(((sinbeta.extract(z)+1.0) * 1000)/2.0)]++;
                                extractMinMax(sinBetaMinMax, sinbeta.extract(z));
                                extractMinMax(a1minMax, a.extract(z));
                                extractMinMax(a2minMax, b.extract(z));
                                extractMinMax(a3minMax, c.extract(z));
                                extractMinMax(a4minMax, d.extract(z));
                            }
*/
			    
                            //std::cout << "(" << a.extract(0) << ", " << b.extract(0) << ", " << c.extract(0) << ", " << d.extract(0) << ")" << std::endl;
                            //std::cout << sinbeta.extract(0) << " - " << sinbetaNewton.extract(0) << ": " << 1 - (sinbetaNewton.extract(0) / sinbeta.extract(0)) << std::endl;

                            TIMER_STOP(timer_quartic);
                            PR0(sinbeta);

                            VECT reflPointX;
                            VECT reflPointY;
                            VECT reflPointZ;

                            // FIXME: pass by value or ref?

                            TIMER_START(timer_transform);
                            /*const std::tuple<VECT, VECT, VECT> reflp =  transform(evecX, evecY, evecZ,
                                                                                dvecX, dvecY, dvecZ,
                                                                                CoCX, CoCY, CoCZ,
                                                                                sinbeta, radius, e);
                            */
                                //used to be transform() function. Put in here because it was inlined
                                //and individual line contributions where not visible in profiler
                            {
                                const VECT nx = (evecY*dvecZ) - (evecZ*dvecY);
                                const VECT ny = (evecZ*dvecX) - (evecX*dvecZ);
                                const VECT nz = (evecX*dvecY) - (evecY*dvecX);

                                const VECT norm = (nx*nx+ny*ny+nz*nz);
        //                        const VECT divnorm = approx_recipr(norm);
                                const VECT norm_sqrt = RichMath::vectorSqrt<VECT>(norm);
                                //nx *= divnorm;
                                //ny *= divnorm;
                                //nz *= divnorm;

                                // FIXME: is there a more performant asin function?
                                // FIXME: where else to add const?
                                // FIXME: alignment
                                // auto beta = asin_f(sinbeta);
                                //const auto beta = VECT(asin(sinbeta.get_low()), asin(sinbeta.get_high()));
                                //const auto beta = asin(sinbeta);

                                const auto a = sinbeta * norm_sqrt;
                                const auto b = (m_constants.c1 - RichMath::vectorSqrt<VECT>(m_constants.c1 - (sinbeta*sinbeta)));
                                const auto enorm = RichMath::vectorDiv<VECT>(radius,(e*norm));
                                // symmetric matrix, diagonal part is 0

                                const std::array<VECT, 9> M = {norm + b*(-nz*nz-ny*ny), a*nz+b*nx*ny, -a*ny+b*nx*nz,
                                                            -a*nz+b*nx*ny, norm+b*(-nx*nx-nz*nz), a*nx+b*ny*nz,
                                                            a*ny+b*nx*nz, -a*nx+b*ny*nz, norm+b*(-ny*ny-nx*nx)};


                                const auto ex = enorm*(evecX*M[0]+evecY*M[3]+evecZ*M[6]);
                                const auto ey = enorm*(evecX*M[1]+evecY*M[4]+evecZ*M[7]);
                                const auto ez = enorm*(evecX*M[2]+evecY*M[5]+evecZ*M[8]);

                                //Prefetches for storing the results at the end of the current loop
                                //There seems to be no difference between prefetch prio 0 and 3
                                //Leaving it at 0 to not pollute L1 too much                            

                                __builtin_prefetch((&(it->sphReflPoint.x())[0]), 1, 0);
                                __builtin_prefetch((&(it->sphReflPoint.y())[0]), 1, 0);
                                __builtin_prefetch((&(it->sphReflPoint.z())[0]), 1, 0);

                                const auto reflPointX = ex + CoCX;
                                const auto reflPointY = ey + CoCY;
                                const auto reflPointZ = ez + CoCZ;                                            

                                reflPointX.store_a(&(it->sphReflPoint.x())[i]);
                                reflPointY.store_a(&(it->sphReflPoint.y())[i]);
                                reflPointZ.store_a(&(it->sphReflPoint.z())[i]);
                            }
                            TIMER_STOP(timer_transform);
                        }
                    }
    //              for(int i = 0; i < 2000; i++)
    //                  std::cout << i << ", " << sinBetaHisto[i] << std::endl;

                    //std::cout << "sinbeta: [" << sinBetaMinMax[0] << ", " << sinBetaMinMax[1] << "]" << std::endl;
                    //std::cout << "a1: [" << a1minMax[0] << ", " << a1minMax[1] << "]" << std::endl;
                    //std::cout << "a2: [" << a2minMax[0] << ", " << a2minMax[1] << "]" << std::endl;
                    //std::cout << "a3: [" << a3minMax[0] << ", " << a3minMax[1] << "]" << std::endl;
                    //std::cout << "a4: [" << a4minMax[0] << ", " << a4minMax[1] << "]" << std::endl;
                }

               // FIXME: performance penalty becaus of tuple?
                //template<class VECTYPE, typename FLOAT_TYPE>
                std::tuple<VECT, VECT, VECT> transform(VECT evecX, VECT evecY, VECT evecZ, VECT dvecX, VECT dvecY, VECT dvecZ,
                                            VECT CoCX, VECT CoCY, VECT CoCZ, VECT sinbeta, VECT radius, VECT e) const {
                        const VECT nx = (evecY*dvecZ) - (evecZ*dvecY);
                        const VECT ny = (evecZ*dvecX) - (evecX*dvecZ);
                        const VECT nz = (evecX*dvecY) - (evecY*dvecX);

                        const VECT norm = (nx*nx+ny*ny+nz*nz);
//                        const VECT divnorm = approx_recipr(norm);
                        const VECT norm_sqrt = RichMath::vectorSqrt<VECT>(norm);
                        //nx *= divnorm;
                        //ny *= divnorm;
                        //nz *= divnorm;

                        // FIXME: is there a more performant asin function?
                        // FIXME: where else to add const?
                        // FIXME: alignment
                        // auto beta = asin_f(sinbeta);
                        //const auto beta = VECT(asin(sinbeta.get_low()), asin(sinbeta.get_high()));
                        //const auto beta = asin(sinbeta);
                        
                        const auto a = sinbeta * norm_sqrt;
                        const auto b = (m_constants.c1 - RichMath::vectorSqrt<VECT>(m_constants.c1 - (sinbeta*sinbeta)));
                        const auto enorm = RichMath::vectorDiv<VECT>(radius,(e*norm));
                        // symmetric matrix, diagonal part is 0

                        const std::array<VECT, 9> M = {norm + b*(-nz*nz-ny*ny), a*nz+b*nx*ny, -a*ny+b*nx*nz,
                                                    -a*nz+b*nx*ny, norm+b*(-nx*nx-nz*nz), a*nx+b*ny*nz,
                                                    a*ny+b*nx*nz, -a*nx+b*ny*nz, norm+b*(-ny*ny-nx*nx)};


                        const auto ex = enorm*(evecX*M[0]+evecY*M[3]+evecZ*M[6]);
                        const auto ey = enorm*(evecX*M[1]+evecY*M[4]+evecZ*M[7]);
                        const auto ez = enorm*(evecX*M[2]+evecY*M[5]+evecZ*M[8]);

                        const VECT reflPointX = ex + CoCX;
                        const VECT reflPointY = ey + CoCY;
                        const VECT reflPointZ = ez + CoCZ;
                        
                        return std::tuple<VECT, VECT, VECT>(reflPointX, reflPointY, reflPointZ);
                }                

                /// The cube root implementaton to use
                //template< class VECTYPE, class FLOAT_TYPE >
                inline FLOAT_TYPE my_cbrt( const FLOAT_TYPE& x ) const
                {
                    // STL
                    //return std::cbrt(x);
                    // LHCbMath FastRoots
                    return FastRoots::cbrt(x);
                }

                //----------------------------------------------------------------------
                /** Solves the quartic equation x^4 + a x^3 + b x^2 + c x + d = 0
                 *
                 *  Optimised to give only solutions needed by RICH optical system
                 *
                 *  Implemented using STL Complex numbers
                 *
                 *  @return The solution needed by the RICH
                 */
                //----------------------------------------------------------------------
                //template< class VECTYPE, class FLOAT_TYPE >
                VECT solve_quartic_RICH ( const VECT& a, const VECT& b, const VECT& c, const VECT& d ) const
                //  const TYPE& a,
                //  const TYPE& b,
                //  const TYPE& c,
                //  const TYPE& d ) const 
                {

                    const VECT r4 = m_constants.c1_4;
                    const VECT q2 = m_constants.c1_2;
                    const VECT q8 = m_constants.c1_8;
                    const VECT q1 = m_constants.c3_8;
                    const VECT q3 = m_constants.c3_16;

                    //const static VECT UU { -( std::sqrt((VECT)3.0) / (VECT)2.0 ) };
                    const VECT UU = m_constants.UU; // - sqrt(3)/2

                    const auto aa = a * a;
                    const auto pp = b - q1 * aa;
                    const auto qq = c - (q2 * a * (b - (r4 * aa)));
                    const auto rr = d - r4 * (a * c - r4 * aa * (b - q3 * aa));
                    const auto rc = q2 * pp;
                    const auto sc = r4 * (r4 * pp * pp - rr);
                    //const auto tc = -pow( q8 * qq, 2 );
                    const auto tc = -(q8 * q8 * qq * qq);

                    const auto qcub = (rc * rc - m_constants.c3 * sc);
                    const auto rcub = (m_constants.c2 * rc * rc * rc - 9 * rc * sc + m_constants.c27 * tc);

                    const auto Q = qcub * m_constants.c1_9;;
                    const auto R = rcub * m_constants.c1_54;;

                    const auto Q3 = Q * Q * Q;
                    const auto R2 = R * R;

                    const auto sgnR = select( R >= 0, -1, 1 );

                    // FIXME: saturated?
                    // const auto toberooted = (TYPE)( abs_saturated(R) + RichMath::vectorSqrt<VECT>(abs_saturated(R2-Q3)) )
                    const auto toberooted = (abs(R) + RichMath::vectorSqrt<VECT>(abs(R2-Q3)) );

                    // FIXME: oder zuerst in normales array, dann load?
                    // FIXME: also for double?
                    // FIXME: magic numbers are eval
                    // FIXME: cbrt with one param of type Vec16f or Vec8d
                    // FIXME: 16 only for float
                    TIMER_START(timer_cbrt);
                    const auto rooted = cbrt(toberooted);
                    //const auto rooted = intrinsic_crbt16(toberooted) 
                    TIMER_STOP(timer_cbrt);

                    const auto A = sgnR * rooted;
                    //TODO: Find out why this doesn't work
                    //const auto A = sign_combine(rooted, R);
                    PR0(A);

                    const auto B = RichMath::vectorDiv<VECT>(Q, A);

                    const auto u1 = -q2 * (A + B) - rc * m_constants.c1_3;
                    // FIXME: saturated or not?
                    //const auto u2 = UU * abs_saturated(A-B);
                    const auto u2 = UU * abs(A-B);
                    const auto V = RichMath::vectorSqrt<VECT>(u1*u1 + u2*u2);
                    // const std::complex<TYPE> w3 = ( abs_satured(V) != 0.0 ? (TYPE)( qq * -0.125 ) / V :
                    //                                std::complex<TYPE>(0,0) );
                    // FIXME: warum abs saturated when compared to 0.0 ??
                    const auto w3r = select(V != 0.0, RichMath::vectorDiv<VECT>((qq * -q8),V), 0.0);
                    //        const TYPE res = std::real(w1) + std::real(w2) + std::real(w3) - (r4*a);
                    const auto res = RichMath::vectorSqrt<VECT>((u1+V) * m_constants.c2) + w3r - (r4*a);
                    // return the final result
                    // FIXME: std::move ?
                    const auto r = select(res >  m_constants.c1,  m_constants.c1, select(res < -m_constants.c1, -m_constants.c1, res));
                    return r;
                }

                VECT newtonRoot(const VECT a0, const VECT &a1, const VECT &a2, const VECT &a3, const VECT &a4) const
                {
                  VECT epsilon;
                  //This is the starting value for the newton iterations
                  //This is the center of the interval of possible values
                  //between 0 and 0.4, which we determined by experimentation
                  //TODO: replace this with most likely value
                  //VECT x(0.2); //Replaced with bisection method for now

                  //Use N steps of bisection method to find starting point for newton
                  VECT l(0);
                  VECT u(0.5);
                  VECT m(0.2);
                  
                  for(int i = 0; i < 3; i++)
                  {
                      //Unfortunately == is not defined for boolean vectors it seems :(
                      //auto ss = (sign_bit(f4(a,b,c,d,m)) == sign_bit(f4(a,b,c,d,l)));
                      //Alternative test
                      
                      auto oppositeSign = sign_bit(f4(a0,a1,a2,a3,a4,m) * f4(a0,a1,a2,a3,a4,l));
                      
                      l = select(oppositeSign, l, m);
                      u = select(oppositeSign, m, u);
                      //std::cout << l.extract(0) << ", " << m.extract(0) << ", " << u.extract(0) << ", " << oppositeSign.extract(0) << std::endl;
                      m = (u + l) * 0.4;
                      //gain = gain * select(oppositeSign, 1.0, -1.0);
                  }
                  
                  //Newton for the rest
                  VECT x = m;
                  
                  //Artificially increase size of newton iteration step
                  //for better convergence. Why 1.04? Totally anecdotal...
                  VECT gain(1.04);
                  for(int i = 0; i < 4; i++)
                  {
                    epsilon = RichMath::vectorDiv<VECT>(f4(a0,a1,a2,a3,a4,x) , df4(a0,a1,a2,a3,x));
                    x = x - gain * epsilon;
                    //std::cout << x.extract(0) << ", " << epsilon.extract(0) << std::endl;
                  }
                  return x;
                }
                
                inline VECT f4(const VECT a0, const VECT &a1, const VECT &a2, const VECT &a3, const VECT &a4, const VECT &x) const
                {
                    //return (x*x*x*x + a * x*x*x + b * x*x + c * x + d);
                    return ((((a0*x)+a1)*x+a2)*x+a3)*x+a4;
                }
                
                inline VECT df4(const VECT a0, const VECT &a1, const VECT &a2, const VECT &a3, const VECT &x) const
                {
                    //return (4*x*x*x + 3*a * x*x + 2*b * x + c);
                    return (((4.0f * a0*x)+3.0f*a1)*x+2.0f*a2)*x+a3;
                }
                
        };

    }
}

#endif // RICHRECPHOTONTOOLS_QuarticSolverNEW_H
