#ifndef _VECTORCLASS_512_H_
#define _VECTORCLASS_512_H_

#pragma GCC diagnostic push
#define MAX_VECTOR_SIZE 512
#include "VectorClass/vectorclass.h"
#pragma GCC diagnostic pop

#endif  /* _VECTORCLASS_512_H_ */

