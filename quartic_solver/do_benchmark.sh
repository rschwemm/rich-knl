#!/bin/bash

CMD=amplxe-cl
#PARAM="-collect memory-access"
PARAM="-collect advanced-hotspots"
PROG="./release 10000000 100"
command amplxe-cl -v || source /afs/cern.ch/sw/IntelSoftware/linux/17-all-setup.sh

if [[ $# -ne 0 ]]; then
	REPORT_NAME="-r $1"
fi

export OMP_NUM_THREADS=240
export LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/ROOT/6.04.02/x86_64-slc6-gcc48-opt/lib

full_cmd="$CMD $PARAM $REPORT_NAME $PROG"
echo $full_cmd
$full_cmd
