/* 
 * File:   richMath.h
 * Author: rainer
 *
 * Created on February 23, 2017, 11:21 AM
 */

#ifndef RICHMATH_H
#define	RICHMATH_H

namespace RichMath
{
    //calculates sqrt(a). Uses fast high precision approximate math if available
    template<class VECTYPE>
    inline VECTYPE vectorSqrt(VECTYPE a)
    {
    #ifdef __AVX512ER__
        return approx_recip(approx_rsqrt(a));
    #else
        return sqrt(a);
    #endif
    }


    
    //calculates a/b. Uses fast high precision approximate math if available
    template<class VECTYPE>
    inline VECTYPE vectorDiv(VECTYPE a, VECTYPE b)
    {
    #ifdef __AVX512ER__
        return a*approx_recipr(b);
    #else         
        return a/b;
    #endif
    }
    
}

#endif	/* RICHMATH_H */

