
//----------------------------------------------------------------------
/** @file QuarticSolverCachelineGCC.h
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2015-01-27
 */
//----------------------------------------------------------------------

#ifndef RICHRECPHOTONTOOLS_QuarticSolverCachelineGCC_H
#define RICHRECPHOTONTOOLS_QuarticSolverCachelineGCC_H

#if not defined STL
// Gaudi
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#else

// STL
//#include <math.h>
#include <type_traits>
//#include <complex>

#endif

// VectorClass
#include "vectorclass512.h"
#include "VectorClass/complexvec.h"
#include "VectorClass/vectormath_trig.h"
#include "VectorClass/vectormath_exp.h"

#include "vectype.h"
#include "cbrt.h"

// Eigen
#include "LHCbMath/EigenTypes.h"
#include <Eigen/Geometry>

// VDT
//#include "vdt/asin.h"

// LHCb Maths
#include "LHCbMath/FastRoots.h"
#include <tuple>

#include "debug.h"

namespace RichCachelineGCC
{
    namespace Rec
    {

        //-----------------------------------------------------------------------------
        /** @class QuarticSolverCachelineGCC
         *
         *  Utility class that implements the solving of the Quartic equation for the RICH
         *
         *  @author Chris Jones         Christopher.Rob.Jones@cern.ch
         *  @date   2015-01-27
         */
        //-----------------------------------------------------------------------------
        class QuarticSolverCachelineGCC
        {

            public:

                // Use eigen types
                typedef LHCb::Math::Eigen::XYZPoint  Point;   ///< Point type
                typedef LHCb::Math::Eigen::XYZVector Vector;  ///< vector type

            public:
                // FIXME: performance penalty becaus of tuple?
                template <typename T>
                inline std::tuple<T, T, T> transform(T evecX, T evecY, T evecZ, T dvecX, T dvecY, T dvecZ,
                                            T CoCX, T CoCY, T CoCZ, T sinbeta, T radius, T e) const {
                        T nx = (evecY*dvecZ) - (evecZ*dvecY);
                        T ny = (evecZ*dvecX) - (evecX*dvecZ);
                        T nz = (evecX*dvecY) - (evecY*dvecX);

                        T norm = (nx*nx+ny*ny+nz*nz);
                        T divnorm = 1.0f/norm;
                        T norm_sqrt = sqrt(norm);
                        nx *= divnorm;
                        ny *= divnorm;
                        nz *= divnorm;

                        // FIXME: is there a more performant asin function?
                        // FIXME: where else to add const?
                        // FIXME: alignment
                        //auto beta = asin(sinbeta);
                        auto beta = vdt::fast_asinf(sinbeta);
                        //auto beta = T(asin(sinbeta.get_low()), asin(sinbeta.get_high()));

                        auto a = sinbeta*norm_sqrt;
                        auto b = (1.0f-cos(beta))*(norm);
                        auto enorm = radius/e;
                        // symmetric matrix, diagonal part is 0

                        const std::array<T, 9> M = {1+b*(-nz*nz-ny*ny), a*nz+b*nx*ny, -a*ny+b*nx*nz,
                                                    -a*nz+b*nx*ny, 1+b*(-nx*nx-nz*nz), a*nx+b*ny*nz,
                                                    a*ny+b*nx*nz, -a*nx+b*ny*nz, 1+b*(-ny*ny-nx*nx)};


                        const auto ex = enorm*(evecX*M[0]+evecY*M[3]+evecZ*M[6]);
                        const auto ey = enorm*(evecX*M[1]+evecY*M[4]+evecZ*M[7]);
                        const auto ez = enorm*(evecX*M[2]+evecY*M[5]+evecZ*M[8]);

                        T reflPointX;
                        T reflPointY;
                        T reflPointZ;

                        reflPointX = ex + CoCX;
                        reflPointY = ey + CoCY;
                        reflPointZ = ez + CoCZ;
                        return std::tuple<T, T, T>(reflPointX, reflPointY, reflPointZ);
                }

                /** Solves the characteristic quartic equation for the RICH optical system.
                 *
                 *  See note LHCB/98-040 RICH section 3 for more details
                 *
                 *  @param emissionPoint Assumed photon emission point on track
                 *  @param CoC           Spherical mirror centre of curvature
                 *  @param virtDetPoint  Virtual detection point
                 *  @param radius        Spherical mirror radius of curvature
                 *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
                 *
                 *  @return boolean indicating status of the quartic solution
                 *  @retval true  Calculation was successful. sphReflPoint is valid.
                 *  @retval false Calculation failed. sphReflPoint is not valid.
                 */
                template< class VECT, class SKALART, size_t DIM >
                    inline void solve( VECTYPE::PhotonReflection<SKALART>& __restrict__ data) const
                    // const VECTYPE& emissionPoint,
                    // const VECTTYPE& CoC,
                    // const VECTYPE& virtDetPoint,
                    // const VECTTYPE radius[],
                    // VECTTYPE& sphReflPoint ) const
                    {
                        #pragma omp simd
                        //#pragma parallel for
                        for(unsigned int i = 0; i < DIM; i++) {
                            // TODO :align 64
                            // FIXME: ueberall const dranmachen?
                            VECT emissionPointVecX = data.emissPnt.x()[i];
                            VECT emissionPointVecY;
                            emissionPointVecY = data.emissPnt.y()[i];
                            VECT emissionPointVecZ;
                            emissionPointVecZ = data.emissPnt.z()[i];

                            VECT CoCX;
                            CoCX = data.centOfCurv.x()[i];
                            VECT CoCY;
                            CoCY = data.centOfCurv.y()[i];
                            VECT CoCZ;
                            CoCZ = data.centOfCurv.z()[i];

                            //const Vector evec( emissionPoint - CoC );
                            VECT evecX = emissionPointVecX - CoCX;
                            VECT evecY = emissionPointVecY - CoCY;
                            VECT evecZ = emissionPointVecZ - CoCZ;

                            // vector from mirror centre of curvature to assumed emission point
                            // const TYPE e2 = evec.dot(evec);
                            VECT e2 = evecX*evecX + evecY*evecY + evecZ*evecZ;

                            // vector from mirror centre of curvature to virtual detection point
                            VECT virtDetPointVecX;
                            virtDetPointVecX = data.virtDetPoint.x()[i];
                            VECT virtDetPointVecY;
                            virtDetPointVecY = data.virtDetPoint.y()[i];
                            VECT virtDetPointVecZ;
                            virtDetPointVecZ = data.virtDetPoint.z()[i];

                            // const Vector dvec( virtDetPoint - CoC );
                            VECT dvecX = virtDetPointVecX - CoCX;
                            VECT dvecY = virtDetPointVecY - CoCY;
                            VECT dvecZ = virtDetPointVecZ - CoCZ;

                            // const TYPE d2 = dvec.dot(dvec);
                            VECT d2 = dvecX*dvecX + dvecY*dvecY + dvecZ*dvecZ;

                            // various quantities needed to create quartic equation
                            // see LHCB/98-040 section 3, equation 3
                            //const auto ed2 = e2 * d2;
                            const VECT ed2 = e2 * d2;
                            const VECT evecDvec = evecX*dvecX + evecY*dvecY + evecZ*dvecZ;


                            // TODO: compare at this point
                            // TODO: div aufwändig? durch * 1/ed2 ersetzen?
                            // const TYPE cosgamma2 = ( ed2 > 0 ? std::pow(evec.dot(dvec),2)/ed2 : 1.0 );
                            // FIXME: durch square = pow of 2 ersetzen?
                            // approx_recipr(a); 
                            VECT cosgamma2 = (evecDvec * evecDvec)/ed2;
                            cosgamma2 = ((ed2 > 0) ? cosgamma2:1.0f);
                            // vectorise 4 square roots into 1
                            //using Vec4x = 
                            //  typename std::conditional<std::is_same<TYPE,float>::value,Vec4f,Vec4d>::type;
                            // const auto tmp_sqrt = sqrt( Vec4x( e2, d2,
                            //                                   cosgamma2 < 1.0 ? 1.0-cosgamma2 : 0.0,
                            //                                   cosgamma2 ) );
                            // const auto e         = tmp_sqrt[0];
                            // const auto d         = tmp_sqrt[1];
                            // const auto singamma  = tmp_sqrt[2];
                            // const auto cosgamma  = tmp_sqrt[3];

                            VECT e = sqrt(e2);
                            VECT d = sqrt(d2);

                            VECT singamma = sqrt(1.0f - cosgamma2);  // TODO: kann es echt > 1.0 werden?
                            VECT cosgamma = sqrt(cosgamma2);

                            // const auto dx        = d * cosgamma;
                            // const auto dy        = d * singamma;
                            // const auto r2        = radius * radius;
                            // const auto dy2       = dy * dy;
                            // const auto edx       = e + dx;
                            VECT dx = d * cosgamma;
                            VECT dy = d * singamma;
                            VECT radius;
                            radius = data.radius[i];
                            VECT r2 = radius * radius;
                            VECT dy2 = dy * dy;
                            VECT edx = e + dx;

                            // Fill array for quartic equation
                            // const auto a0      =     4.0 * ed2;
                            // const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 : std::numeric_limits<TYPE>::max() );
                            // const auto dyrad2  =     2.0 * dy * radius;
                            // const auto a1      = - ( 2.0 * dyrad2 * e2 ) * inv_a0;
                            // const auto a2      =   ( (dy2 * r2) + ( edx * edx * r2 ) - a0 ) * inv_a0;
                            // const auto a3      =   ( dyrad2 * e * (e-dx) ) * inv_a0;
                            // const auto a4      =   ( ( e2 - r2 ) * dy2 ) * inv_a0;

                            VECT a0 = 4.0f * ed2;
                            // FIXME: warum floatmax, statt inf?
                            VECT maxval  =  std::numeric_limits<SKALART>::max();
                            VECT inv_a0    = ((a0 > 0)? 1.0f/a0: maxval);
                            VECT dyrad2    = 2.0f * dy *radius;
                            VECT a1        = -( 2.0f * dyrad2 * e2 ) * inv_a0;
                            VECT a2        = ( ( dy2* r2 ) + (edx *edx * r2) - a0 ) * inv_a0;
                            VECT a3        = ( (dyrad2 * e * (e-dx)) * inv_a0);
                            VECT a4        = ( (e2 - r2) * dy2 ) * inv_a0;

                            // use simplified RICH version of quartic solver
                            TIMER_START(timer_quartic);
                            auto sinbeta = solve_quartic_RICH<VECT, SKALART>( a1, a2, a3, a4);
                            TIMER_STOP(timer_quartic);
                            PR(sinbeta);

                            VECT reflPointX  __attribute__((__aligned__(64)));
                            VECT reflPointY  __attribute__((__aligned__(64)));
                            VECT reflPointZ __attribute__((__aligned__(64)));

                            // FIXME: pass by value or ref?

                            TIMER_START(timer_transform);
                            std::tuple<VECT, VECT, VECT> reflp =  transform(evecX, evecY, evecZ,
                                                                                dvecX, dvecY, dvecZ,
                                                                                CoCX, CoCY, CoCZ,
                                                                                sinbeta, radius, e);
                            TIMER_STOP(timer_transform);

                            std::tie (reflPointX, reflPointY, reflPointZ) = reflp;
                            // (normalised) normal vector to reflection plane
                            // auto n = evec.cross3(dvec);
                            //   n /= std::sqrt( n.dot(n) );

                            /*
                            // construct rotation transformation
                            // Set vector magnitude to radius
                            // rotate vector and update reflection point
                            typedef Eigen::Matrix< TYPE , 3 , 1 > Eigen3Vector;
                            const Eigen::AngleAxis<TYPE> angleaxis( vdt::fast_asinf(sinbeta),
                            Eigen3Vector(n[0],n[1],n[2]) );
                            sphReflPoint = ( CoC + Gaudi::XYZVector( angleaxis *
                            Eigen3Vector(evec[0],evec[1],evec[2]) *
                            ( radius / e ) ) );
                            */

                            first = false;

                            data.sphReflPoint.x()[i] = reflPointX;
                            data.sphReflPoint.y()[i] = reflPointY;
                            data.sphReflPoint.z()[i] = reflPointZ;
                        }
                    }


                /// The cube root implementaton to use
                template < class TYPE >
                    inline TYPE my_cbrt( const TYPE& x ) const
                    {
                        // STL
                        //return std::cbrt(x);
                        // LHCbMath FastRoots
                        return FastRoots::cbrt(x);
                    }

                //----------------------------------------------------------------------
                /** Solves the quartic equation x^4 + a x^3 + b x^2 + c x + d = 0
                 *
                 *  Optimised to give only solutions needed by RICH optical system
                 *
                 *  Implemented using STL Complex numbers
                 *
                 *  @return The solution needed by the RICH
                 */
                //----------------------------------------------------------------------
                template < class VECT , class SKALART>
                    inline VECT solve_quartic_RICH ( VECT& __restrict__ a, VECT& __restrict__ b, VECT& __restrict__ c, VECT& __restrict__ d ) const
                    //  const TYPE& a,
                    //  const TYPE& b,
                    //  const TYPE& c,
                    //  const TYPE& d ) const 
                    {

                        const static VECT r4 = 1.0f / 4.0f;
                        const static VECT q2 = 1.0f / 2.0f;
                        const static VECT q8 = 1.0f / 8.0f;
                        const static VECT q1 = 3.0f / 8.0f;
                        const static VECT q3 = 3.0f / 16.0f;
                        //const static VECT UU { -( std::sqrt((VECT)3.0) / (VECT)2.0 ) };
                        const static VECT UU = -0.866025404; // - sqrt(3)/2

                        const auto aa = a * a;
                        const auto pp = b - q1 * aa;
                        const auto qq = c - (q2 * a * (b - (r4 * aa)));
                        const auto rr = d - r4 * (a * c - r4 * aa * (b - q3 * aa));
                        const auto rc = q2 * pp;
                        const auto sc = r4 * (r4 * pp * pp - rr);
                        const auto tc = -q8 * q8 * qq * qq;

                        const auto qcub = (rc * rc - 3 * sc);
                        const auto rcub = (2.0 * rc * rc * rc - 9 * rc * sc + 27.0 * tc);

                        const auto Q = qcub / 9.0;
                        const auto R = rcub / 54.0;

                        const auto Q3 = Q * Q * Q;
                        const auto R2 = R * R;

                        const auto sgnR = ( R >= 0? -1: 1 );

                        // FIXME: saturated?
                        // const auto toberooted = (TYPE)( abs_saturated(R) + sqrt(abs_saturated(R2-Q3)) )
                        const auto toberooted = (abs(R) + sqrt(abs(R2-Q3)) );

                        // FIXME: oder zuerst in normales array, dann load?
                        // FIXME: also for double?
                        // FIXME: magic numbers are eval
                        // FIXME: cbrt with one param of type Vec16f or Vec8d
                        // FIXME: 16 only for float
                        TIMER_START(timer_cbrt);
                        const auto rooted = cbrt(toberooted);
                        TIMER_STOP(timer_cbrt);

                        const auto A = sgnR * rooted;
                        PR(A);

                        const auto B = Q / A;

                        const auto u1 = -0.5 * (A + B) - rc / 3.0;
                        // FIXME: saturated or not?
                        //const auto u2 = UU * abs_saturated(A-B);
                        const auto u2 = UU * abs(A-B);
                        const auto V = sqrt(u1*u1 + u2*u2);
                        // const std::complex<TYPE> w3 = ( abs_satured(V) != 0.0 ? (TYPE)( qq * -0.125 ) / V :
                        //                                std::complex<TYPE>(0,0) );
                        // FIXME: warum abs saturated when compared to 0.0 ??
                        const auto w3r = ((V != 0.0)? (qq * -0.125)/V : 0.0);
                        //        const TYPE res = std::real(w1) + std::real(w2) + std::real(w3) - (r4*a);
                        const auto res = sqrt((u1+V)*2) + w3r - (r4*a);
                        // return the final result
                        // FIXME: std::move ?
                        const auto r = ((res >  1.0)?  1.0: ((res < -1.0)? -1.0: res));
                        return r;
                    }

        };

    }
}

#endif // RICHRECPHOTONTOOLS_QuarticSolverNEW_H
