#!/bin/bash

DPATH=./perf
PPATH=./perf/FlameGraph

NAME=$1
CMD=$2

echo $NAME
echo $CMD

perf record -F 99 -ag -- $CMD -o $DPATH/perf.data
perf script | $PPATH/stackcollapse-perf.pl > $DPATH/out.$NAME-folded
cat $DPATH/out.$NAME-folded | $PPATH/flamegraph.pl > $DPATH/$NAME.svg

