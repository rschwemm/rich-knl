
#ifndef __VECTYPE__
#define __VECTYPE__

#include <vector>
#include <array>
#include "aligned_allocator.h"

namespace VECTYPE {
    template <typename T, std::size_t DIM = 16>
        class XYZPoints
        {
            public:
                enum { N = DIM };
                static constexpr std::size_t size() { return N; }
                typedef T value_type;
                typedef std::array<T, N> vec_type;
            private:
                std::array<vec_type, 3> vec;
            public:
                vec_type& x() { return vec[0]; }
                const vec_type& x() const { return vec[0]; }
                vec_type& y() { return vec[1]; }
                const vec_type& y() const { return vec[1]; }
                vec_type& z() { return vec[2]; }
                const vec_type& z() const { return vec[2]; }
        };

    template <typename T, std::size_t DIM = 16>
        class PhotonReflection
        {
            public:
                typedef typename XYZPoints<T, DIM>::vec_type vector;
            public:
                XYZPoints<T, DIM> emissPnt;
                XYZPoints<T, DIM> centOfCurv;
                XYZPoints<T, DIM> virtDetPoint;
                // FIXME: rausnehmen, eigene datenstruktur:
                XYZPoints<T, DIM> sphReflPoint;
                // FIXME: align data to cache line boundary
                // FIXME: how to pass array size properly?
                std::array<T,DIM> radius;
            public:
                void printPhotonData(size_t offset)
                {
                    std::cout << "emission point       : (" << emissPnt.x()[offset] << ", " << emissPnt.y()[offset] << ", " << emissPnt.z()[offset] << ")" << std::endl;
                    std::cout << "center of curvature  : (" << centOfCurv.x()[offset] << ", " << centOfCurv.y()[offset] << ", " << centOfCurv.z()[offset] << ")" << std::endl;
                    std::cout << "virt. detection point: (" << virtDetPoint.x()[offset] << ", " << virtDetPoint.y()[offset] << ", " << virtDetPoint.z()[offset] << ")" << std::endl;
                    std::cout << "reflection point     : (" << sphReflPoint.x()[offset] << ", " << sphReflPoint.y()[offset] << ", " << sphReflPoint.z()[offset] << ")" << std::endl;
                    std::cout << "radius               : " << radius[offset] << std::endl;                    
                }
        };

    template <typename T, std::size_t DIM = 16>
    using PhotonReflections = std::vector<PhotonReflection<T, DIM>,  aligned_allocator< PhotonReflection<T, DIM>, 64>>;

    template < class T>
        std::ostream& operator<< (std::ostream& os, const XYZPoints<T>& vp) {
            for (int i = 0; i < vp.size(); i++) {
                // FIXME: will ich nicht eher die zeilen sehen?
                // os << i << ": x=" << vp.x()[i] << ", y=" << vp.y()[i] << ", z=" << vp.z()[i] << std::endl;
                os << "(" << vp.x()[i] << "," << vp.y()[i] << "," << vp.z()[i] << ")" << std::endl;
            }
            return os;
        }

    template < typename T>
        std::ostream& operator<<(std::ostream& os, const VECTYPE::PhotonReflection<T> & dat) {
            return os   << "emissPnt: " << dat.emissPnt
                << "centOfCurv:  " << dat.centOfCurv
                << "virtDetPoint:  " << dat.virtDetPoint
                << "radius:  " << dat.radius
                << std::endl;
        }

    template < typename T>
        std::ostream& operator<<(std::ostream& os, const typename VECTYPE::PhotonReflections<T>::Vector & vt) {
            std::cout << "VECTYPE::Data::Vector " << std::endl;
            for (auto const& val : vt) {
                os << val << std::endl;
            }
            return os;
        }
    
    //Complete SOA structures
    template<typename T, std::size_t DIM = 16>
    class XYZArrays
    {
    public:
        static constexpr std::size_t size() { return DIM; }
    private:
        T* m_x = 0;
        T* m_y = 0;
        T* m_z = 0;
    public:
        T* x() { return m_x; }
        const T* x() const { return m_x; }
        T*y() { return m_y; }
        const T* y() const { return m_y; }
        T* z() { return m_z; }
        const T* z() const { return m_z; }
        
        bool allocateSpace(std::size_t nElements)
        {
            this->m_x = (T*)aligned_alloc(64, nElements * sizeof(T));
            this->m_y = (T*)aligned_alloc(64, nElements * sizeof(T));
            this->m_z = (T*)aligned_alloc(64, nElements * sizeof(T));
            
            //Return false if there is a problem with allocation
            if(!this->m_x || !this->m_y || !this->m_z)
                return false;
            return true;
        }
        void freeSpace()
        {
            free(this->x);
            free(this->y);
            free(this->z);
        }
    };
    
    template <typename T, std::size_t DIM = 16>
        class PhotonReflectionArrays
        {
            public:
                typedef typename XYZPoints<T, DIM>::vec_type vector;
            public:
                XYZArrays<T, DIM> emissPnt;
                XYZArrays<T, DIM> centOfCurv;
                XYZArrays<T, DIM> virtDetPoint;
                XYZArrays<T, DIM> sphReflPoint;
                T* radius;
            public:
                bool allocateSpace(std::size_t nElements)
                {
                    bool res = false;
                    res &= emissPnt.allocateSpace(nElements);
                    res &= centOfCurv.allocateSpace(nElements);
                    res &= virtDetPoint.allocateSpace(nElements);
                    res &= sphReflPoint.allocateSpace(nElements);
                    
                    radius = (T*)aligned_alloc(64, nElements * sizeof(T));
                    res &= !(!radius);
                    
                    return res;
                }
                
                void printPhotonData(size_t offset)
                {
                    std::cout << "emission point       : (" << emissPnt.x()[offset] << ", " << emissPnt.y()[offset] << ", " << emissPnt.z()[offset] << ")" << std::endl;
                    std::cout << "center of curvature  : (" << centOfCurv.x()[offset] << ", " << centOfCurv.y()[offset] << ", " << centOfCurv.z()[offset] << ")" << std::endl;
                    std::cout << "virt. detection point: (" << virtDetPoint.x()[offset] << ", " << virtDetPoint.y()[offset] << ", " << virtDetPoint.z()[offset] << ")" << std::endl;
                    std::cout << "reflection point     : (" << sphReflPoint.x()[offset] << ", " << sphReflPoint.y()[offset] << ", " << sphReflPoint.z()[offset] << ")" << std::endl;
                    std::cout << "radius               : " << radius[offset] << std::endl;                    
                }
        };
    
}

#endif  /*  __VECTYPE__ */
