
//----------------------------------------------------------------------
/** @file QuarticSolver.h
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2015-01-27
 */
//----------------------------------------------------------------------

#ifndef RICHRECPHOTONTOOLS_QuarticSolver_MathChanges_H
#define RICHRECPHOTONTOOLS_QuarticSolver_MathChanges_H 1

#if not defined STL
// Gaudi
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

// VectorClass
#include "VectorClass/vectorclass.h"
#include "VectorClass/complexvec.h"


#else

// STL
#include <math.h>
#include <type_traits>
//#include <complex>

#endif

// Eigen
#include "LHCbMath/EigenTypes.h"
#include <Eigen/Geometry>

// VDT
#include "vdt/asin.h"

// LHCb Maths
#include "LHCbMath/FastRoots.h"

#include "debug.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class QuarticSolver
     *
     *  Utility class that implements the solving of the Quartic equation for the RICH
     *  Based on original code by Chris Jones
     *
     *  @author Christina Quast, Rainer Schwemmer
     *  @date   2017-02-03
     */
    //-----------------------------------------------------------------------------
    class QuarticSolverNewton
    {

    public:

      // Use eigen types
      typedef LHCb::Math::Eigen::XYZPoint  Point;   ///< Point type
      typedef LHCb::Math::Eigen::XYZVector Vector;  ///< vector type

    public:

      /** Solves the characteristic quartic equation for the RICH optical system.
       *
       *  See note LHCB/98-040 RICH section 3 for more details
       *
       *  @param emissionPoint Assumed photon emission point on track
       *  @param CoC           Spherical mirror centre of curvature
       *  @param virtDetPoint  Virtual detection point
       *  @param radius        Spherical mirror radius of curvature
       *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
       *
       *  @return boolean indicating status of the quartic solution
       *  @retval true  Calculation was successful. sphReflPoint is valid.
       *  @retval false Calculation failed. sphReflPoint is not valid.
       */
      template< class TYPE >
      inline void solve( const Gaudi::XYZPoint& emissionPoint,
                         const Gaudi::XYZPoint& CoC,
                         const Gaudi::XYZPoint& virtDetPoint,
                         const TYPE radius,
                         Gaudi::XYZPoint& sphReflPoint ) const
      {

        // typedefs vectorised types
        typedef Eigen::Matrix< TYPE , 3 , 1 > Eigen3Vector;
        using Vec4x = 
          typename std::conditional<std::is_same<TYPE,float>::value,Vec4f,Vec4d>::type;

        // vector from mirror centre of curvature to assumed emission point
        const Vector evec( emissionPoint - CoC );
        const TYPE e2 = evec.dot(evec);

        // vector from mirror centre of curvature to virtual detection point
        const Vector dvec( virtDetPoint - CoC );
        const TYPE d2 = dvec.dot(dvec);

        // various quantities needed to create quartic equation
        // see LHCB/98-040 section 3, equation 3
        const TYPE ed2 = e2 * d2;
        //std::pow uses internal loop and causes branch misses and other inefficiencies
        //There might be a constant exponent pow function too, but i'm too lazy to check.
        //const TYPE cosgamma2 = ( ed2 > 0 ? std::pow(evec.dot(dvec),2)/ed2 : 1.0 );
        const TYPE cosgamma2 = ( ed2 > 0.0f ? evec.dot(dvec)*evec.dot(dvec)/ed2 : 1.0f );
        // vectorise 4 square roots into 1
        //TODO: This vectorized sqrt is questionable. There is a lot of overhead in gathering
        //The different components via unaligned loads. It's probably not
        //Faster than just computing the 4 square roots individually.
        const auto tmp_sqrt = sqrt( Vec4x( e2, d2,
                                           cosgamma2 < 1.0f ? 1.0f-cosgamma2 : 0.0f,
                                           cosgamma2 ) );
        const TYPE e         = tmp_sqrt[0];
        const TYPE d         = tmp_sqrt[1];
        const TYPE singamma  = tmp_sqrt[2];
        const TYPE cosgamma  = tmp_sqrt[3];

        const TYPE dx        = d * cosgamma;
        const TYPE dy        = d * singamma;
        const TYPE r2        = radius * radius;
        const TYPE dy2       = dy * dy;
        const TYPE edx       = e + dx;

        // Fill array for quartic equation
        const auto a0      =     4.0f * ed2;
        //Newton solver doesn't care about a0 being not 1.0. Remove costly division and several multiplies.
        //This has some downsides though. The a-values are hovering around a numerical value of 10^15. single
        //precision float max is 10^37. A single square and some multiplies will push it over the limit of what
        //single precision float can handle. It's ok for the newton method, but Halley or higher order Housholder
        //will fail without this normalization.
        //const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 : std::numeric_limits<TYPE>::max() );
        const TYPE dyrad2  =     2.0f * dy * radius;
        const TYPE a1      = - ( 2.0f * dyrad2 * e2 );// * inv_a0;
        const TYPE a2      =   ( (dy2 * r2) + ( edx * edx * r2 ) - a0 );// * inv_a0;
        const TYPE a3      =   ( dyrad2 * e * (e-dx) );// * inv_a0;
        const TYPE a4      =   ( ( e2 - r2 ) * dy2 );// * inv_a0;

        // use simplified RICH version of quartic solver
//        const auto sinbeta = solve_quartic_RICH<TYPE>( a1, // a
//                                                       a2, // b
//                                                       a3, // c
//                                                       a4  // d
//                                                       );

        //Use optimized newton solver on quartic equation. 
        const auto sinbeta = solve_quartic_newton_RICH(a0, a1, a2, a3, a4);
        
        //TODO: This method should be better but has problems still for some reasons
        //const auto sinbeta = solve_quartic_housholder_RICH<TYPE, 3>(a1, a2, a3, a4);
      
        PR(evec);
        PR(dvec);
        PR(CoC);
        PR(sinbeta);
        PR(radius);
        PR(e);

        // construct rotation transformation
        // Set vector magnitude to radius
        // rotate vector and update reflection point
        //rotation matrix uses sin(beta) and cos(beta) to perform rotation
        //even fast_asinf (which is only single precision and defeats the purpose
        //of this class being templatable to double btw) is still too slow 
        //plus there is a cos and sin call inside AngleAxis ...
        //We can do much better by just using the cos(beta) we already have to calculate
        //sin(beta) and do our own rotation. On top of that we rotate non-normalized and save several
        //Divisions by normalizing only once at the very end
        //Again, care has to be taken since we are close to float_max here without immediate normalization.
        //As far as we have tried with extreme values in the rich coordinate systems this is fine.
        //const Eigen::AngleAxis<TYPE> angleaxis( vdt::fast_asinf(sinbeta),
        //                                        Eigen3Vector(n[0],n[1],n[2]) );
        const TYPE nx = (evec[1]*dvec[2]) - (evec[2]*dvec[1]);
        const TYPE ny = (evec[2]*dvec[0]) - (evec[0]*dvec[2]);
        const TYPE nz = (evec[0]*dvec[1]) - (evec[1]*dvec[0]);
        //const auto n = evec.cross3(dvec);      
        //const auto norm = n.dot(n);
        const TYPE norm = nx*nx + ny*ny + nz*nz;
        const TYPE norm_sqrt = std::sqrt(norm);        
        
        const TYPE a = sinbeta * norm_sqrt;
        const TYPE b = (1.0f - std::sqrt(1.0 - (sinbeta*sinbeta))); // <--(1-cos(beta))
        const TYPE enorm = radius/(e*norm);
        
        //Perform non-normalized rotation
        const std::array<TYPE, 9> M = {norm + b*(-nz*nz-ny*ny), a*nz+b*nx*ny,         -a*ny+b*nx*nz,
                                       -a*nz+b*nx*ny,           norm+b*(-nx*nx-nz*nz), a*nx+b*ny*nz,
                                        a*ny+b*nx*nz,           -a*nx+b*ny*nz,         norm+b*(-ny*ny-nx*nx)};


        //re-normalize rotation and scale to radius in one step
        const TYPE ex = enorm*(evec[0]*M[0]+evec[1]*M[3]+evec[2]*M[6]);
        const TYPE ey = enorm*(evec[0]*M[1]+evec[1]*M[4]+evec[2]*M[7]);
        const TYPE ez = enorm*(evec[0]*M[2]+evec[1]*M[5]+evec[2]*M[8]);

        
#ifdef DEBUG_SOLVE
        auto asinbeta = vdt::fast_asinf(sinbeta);
        PR(asinbeta);
        auto tmp = angleaxis * Eigen3Vector(evec[0],evec[1],evec[2]);
        PR(tmp);
        auto tmp2 = Gaudi::XYZVector( tmp);
        auto tmp3 = tmp2*(radius/e);
        PR(tmp3);
#endif

        sphReflPoint = ( CoC + Gaudi::XYZVector( ex, ey, ez ) );

        PR(sphReflPoint);
      }

    public:

      /// The cube root implementaton to use
      template < class TYPE >
      inline TYPE my_cbrt( const TYPE& x ) const
      {
        // STL
        //return std::cbrt(x);
        // LHCbMath FastRoots
        return FastRoots::cbrt(x);
      }

      //----------------------------------------------------------------------
      /** Solves the quartic equation x^4 + a x^3 + b x^2 + c x + d = 0
       *
       *  Optimised to give only solutions needed by RICH optical system
       *
       *  Implemented using STL Complex numbers
       *
       *  @return The solution needed by the RICH
       */
      //----------------------------------------------------------------------
      template < class TYPE >
      inline TYPE solve_quartic_RICH ( const TYPE& a,
                                       const TYPE& b,
                                       const TYPE& c,
                                       const TYPE& d ) const 
      {
        PR(a);
        PR(b);
        PR(c);
        PR(d);

        constexpr const TYPE r4 = 1.0 / 4.0;
        constexpr const TYPE q2 = 1.0 / 2.0;
        constexpr const TYPE q8 = 1.0 / 8.0;
        constexpr const TYPE q1 = 3.0 / 8.0;
        constexpr const TYPE q3 = 3.0 / 16.0;
        //const TYPE UU { -( std::sqrt((TYPE)3.0) / (TYPE)2.0 ) };
        constexpr const TYPE UU = -0.866025404; // - sqrt(3)/2

#ifdef DEBUG
        PR(r4);
        PR(q2);
        PR(q8);
        PR(q1);
        PR(q3);
        PR(UU);
#endif

        const auto aa = a * a;
        const auto pp = b - q1 * aa;
        const auto qq = c - q2 * a * (b - r4 * aa);
        const auto rr = d - r4 * (a * c - r4 * aa * (b - q3 * aa));
        const auto rc = q2 * pp;
        const auto sc = r4 * (r4 * pp * pp - rr);
        const auto tc = - std::pow( q8 * qq, 2 );

        const auto qcub = (rc * rc - 3 * sc);
        const auto rcub = (2.0 * rc * rc * rc - 9 * rc * sc + 27.0 * tc);

        const auto Q = qcub / 9.0;
        const auto R = rcub / 54.0;

        const auto Q3 = Q * Q * Q;
        const auto R2 = R * R;

        const auto sgnR = ( R >= 0 ? -1 : 1 );

        const auto A = sgnR * my_cbrt( (TYPE)( fabs(R) + std::sqrt(fabs(R2-Q3)) ) );

        const auto B = Q / A;

        const auto u1 = -0.5 * (A + B) - rc / 3.0;
        const auto u2 = UU * fabs(A-B);

#ifdef DEBUG
        PR(pp);
        PR(qq);
        PR(rr);
        PR(rc);
        PR(sc);
        PR(tc);
        PR(qcub);
        PR(Q);
        PR(R);
        PR(sgnR);
        PR(A);
        PR(B);
        PR(u1);
        PR(u2);
#endif

#ifdef STL
        // // Implementation using STL classes
        const auto w1 = std::sqrt( std::complex<TYPE>(u1, u2) );
        const auto w2 = std::sqrt( std::complex<TYPE>(u1,-u2) );
        const auto  V = w1 * w2;
        const std::complex<TYPE> w3 = ( std::abs(V) != 0.0 ? (TYPE)( qq * -0.125 ) / V :
                                        std::complex<TYPE>(0,0) );
        const TYPE res = std::real(w1) + std::real(w2) + std::real(w3) - (r4*a);

        // Vectorised implementation using VectorClass
        // (Much) faster, but currently causes problems with clang

#else
        using Complex4x = 
          typename std::conditional<std::is_same<TYPE,float>::value,Complex4f,Complex4d>::type;
        using Complex2x = 
          typename std::conditional<std::is_same<TYPE,float>::value,Complex2f,Complex2d>::type;
        const Complex4x  W = sqrt( Complex4x(u1,u2,u1,-u2) );
        const auto       V = W.get_low() * W.get_high();
        const auto      w3 = ( fabs(V.extract(0)) > 0 || fabs(V.extract(1)) > 0 ? 
                               ( qq * -0.125 ) / V : Complex2x(0,0) );
        const TYPE     res = W.extract(0) + W.extract(2) + w3.extract(0) - (r4*a);
#ifdef DEBUG
        PR(W.extract(0));
        PR(W.extract(1));
        PR(w3.extract(0));
        PR(w3.extract(1));
        PR(res);
#endif /* DEBUG */
#endif



        // return the final result
        return ( res >  1.0 ?  1.0 :
                 res < -1.0 ? -1.0 :
                 res );
      }
      
      //A newton iteration solver for the Rich quartic equation
      //Since the polynomial that is evaluated here is extremely constrained
      //(root is in small interval, one root guaranteed), we can use a much more
      //efficient approximation (which still has the same precision) instead of the
      //full blown mathematically absolute correct method and still end up with
      //usable results
      template < class TYPE >
      inline TYPE f4(const TYPE &a0, const TYPE &a1, const TYPE &a2, const TYPE &a3, const TYPE &a4, TYPE &x) const
      {
          //return (a0 * x*x*x*x + a1 * x*x*x + a2 * x*x + a3 * x + a4);
          //A bit more FMA friendly
          return ((((a0*x)+a1)*x+a2)*x+a3)*x+a4;
      }
      
      template < class TYPE >
      inline TYPE df4(const TYPE &a0, const TYPE &a1, const TYPE &a2, const TYPE &a3, TYPE &x) const
      {
          //return (4.0f*a0 * x*x*x + 3.0f*a1 * x*x + 2.0f*a2 * x + a3);
          return (((4.0f * a0*x)+3.0f*a1)*x+2.0f*a2)*x+a3;
      }    

      //Horner's method to evaluate the polynomial and its derivatives with as little math operations as
      //possible. We use a template here to allow the compiler to unroll the for loops and produce code
      //that is free from branches and optimized for the grade of polynomial and derivatives as necessary.
      template < class TYPE, std::size_t ORDER = 4, std::size_t DIFFGRADE = 3 >
      inline void evalPolyHorner(const TYPE (&a)[ORDER+1], TYPE (&res)[DIFFGRADE+1], TYPE x) const
      {
        for(unsigned int i = 0; i <= DIFFGRADE; i++)
        {
          res[i] = a[0];
        }

        for(unsigned int j = 1; j <= ORDER; j++)
        {
          res[0] = res[0] * x + a[j];
          int l = (ORDER - j) > DIFFGRADE ? DIFFGRADE : ORDER - j;
          for (int i = 1; i <= l ; i++)
          {
            res[i] = res[i] * x + res[i-1];
          }
        }

        //TODO: Check assembly to see if this is optimized away if DIFFGRADE is <= 2
        float l = 1.0;
        for(unsigned int i = 2; i <= DIFFGRADE; i++)
        {
          l *= i;
          res[i] = res[i] * l;
        }
          
      }
      
      //3rd grade Housholder's method for iteratively finding the root of a function. Tests have shown that we seem to be
      //already too constrained in our polynomial and input data that we are not gaining anything over newton. In fact it seems
      //to make performance worse.
      //TODO: find out why performance of this is so bad. We might be missing something. It should converge much faster than newton.
      template < class TYPE, std::size_t ITER = 3 >
      inline TYPE householder(const TYPE (&a)[5], TYPE x0) const
      {
        TYPE res[4];
        for(unsigned int i = 0; i < ITER; i++)
        {
          evalPolyHorner<TYPE, 4, 3>(a, res, x0);
          x0  =  x0 -   ((6.0 * res[0]*res[1]*res[1] - 3.0 * res[0]*res[0]*res[2])/
                         (6.0 * res[1]*res[1]*res[1] - 6.0 * res[0]*res[1]*res[2] + res[0]*res[0]*res[3]));
          //std::cout << x0 << ": " << res[0] << ", " << res[1] << ", " << res[2] << ", "<< res[3] << ", " << std::endl;

        }
        return x0;
      }
      
     //Use Housholder's method to solve the rich quartic equation. Important! If this function is used, the coefficients of the
     //polynomial have to be normalized. They are typically around 10^15 and floating point overflows will occur in here if these
     //large values are used.
     template < class TYPE , std::size_t ITER = 3 >
     inline TYPE solve_quartic_housholder_RICH( const TYPE& a0,
                                       const TYPE& a1,
                                       const TYPE& a2,
                                       const TYPE& a3) const      
     {
         //Starting value for housholder iteration. Empirically values seem to be
         //between 0 0and 0.4 so we chose the value in the middle as starting point
         TYPE x0 = 0.2f;
       
         TYPE a[5] = {1.0f, a0, a1, a2, a3};
         //std::cout << "Polynomial: " << std::endl;
         //std::cout << a[0] << ", " << a[1] << ", " << a[2] << ", " << a[3] << ", " << a[4] << std::endl;
         x0 = householder<TYPE, ITER>(a, x0);
        
         return x0;
     }
      
      //Newton-Rhapson method for calculating the root of the rich polynomial. It uses the bisection method in the beginning
      //to get close enough to the root to allow the second stage newton method to converge faster. After 4 iterations of newton
      //precision is as good as single precision floating point will get you. We have introduced a few tuning parameters like the
      //newton gain factor and a slightly skewed bisection division, which in this particular case help to speed things up.
      //TODO: Once we are happy with the number of newton and bisection iterations, this function should be templated to the number of
      //these iterations to allow loop unrolling and elimination of unnecessary branching
      //TODO: These tuning parameters have been found by low effort experimentation on random input data. A more detailed 
      //study should be done with real data to find the best values
      template < class TYPE >
      inline TYPE solve_quartic_newton_RICH( const TYPE& a0,
                                       const TYPE& a1,
                                       const TYPE& a2,
                                       const TYPE& a3,
                                       const TYPE& a4) const
      {
        TYPE epsilon;       
        //Use N steps of bisection method to find starting point for newton
        TYPE l(0);
        TYPE u(0.5);
        TYPE m(0.2); //We start a bit off center since the distribution of roots tends to be more to the left side
        const TYPE a[5] = {a0, a1, a2, a3, a4};
        TYPE res[2];
        for(int i = 0; i < 3; i++)
        {
            auto oppositeSign = std::signbit(f4(a0,a1,a2,a3,a4,m) * f4(a0,a1,a2,a3,a4,l));

            l = oppositeSign ? l : m;
            u = oppositeSign ? m : u;
            //std::cout << l.extract(0) << ", " << m.extract(0) << ", " << u.extract(0) << ", " << oppositeSign.extract(0) << std::endl;
            //0.4 instead of 0.5 to speed up convergence. Most roots seem to be closer to 0 than to the extreme end
            m = (u + l) * 0.4;
        }

        //Newton for the rest
        TYPE x = m;
        
        //Most of the times we are approaching the root of the polynomial from one side
        //and fall short by a certain fraction. This fraction seems to be around 1.04 of the
        //quotient which is subtracted from x. By scaling it up, we take bigger steps towards
        //the root and thus converge faster.
        //TODO: study this factor more closely it's pure guesswork right now. We might get          
        //away with 3 iterations if we can find an exact value
        const TYPE gain = 1.04;
        
        for(int i = 0; i < 4; i++)
        {
          evalPolyHorner<TYPE, 4, 1>(a, res, x);
          //epsilon = f4(a0,a1,a2,a3,a4,x) / df4(a0,a1,a2,a3,x);
          epsilon = res[0] / res[1];
          x = x - gain * epsilon;
        }
        return x;
      }
    };

  }
}

#endif // RICHRECPHOTONTOOLS_QuarticSolver_MathChanges_H
