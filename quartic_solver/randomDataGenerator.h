/* 
 * File:   randomDataGenerator.h
 * Author: rainer
 *
 * Created on February 1, 2017, 4:45 PM
 */

#ifndef RANDOMDATAGENERATOR_H
#define	RANDOMDATAGENERATOR_H

// STL
#include <random>
#include <vector>
#include <math.h>

// Gaudi
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

//Our stuff
#include "vectype.h"


class PhotonData {
public:
  typedef std::vector<PhotonData> Vector;
public:
  Gaudi::XYZPoint emissPnt;
  Gaudi::XYZPoint centOfCurv;
  Gaudi::XYZPoint virtDetPoint;
  double           radius;
  Gaudi::XYZPoint sphReflPoint;
public:  
  void printPhotonData();
};

template <class FLOAT_TYPE, std::size_t BATCH_SIZE = 16>
class RandomDataGenerator {
public:
    RandomDataGenerator(uint64_t nPhotons, bool useAOSData = false, bool useHalfAOSData = true, bool useFullAOSData = false);
    PhotonData* generatePhoton(uint64_t seed);
    void generateData();
    void releasePhoton(PhotonData* photonData);
    void shuffleAOS(const PhotonData* photonData, uint64_t offset, VECTYPE::PhotonReflections<FLOAT_TYPE, BATCH_SIZE>& targetData);
    void shuffleFullAOS(const PhotonData* photonData, uint64_t offset, VECTYPE::PhotonReflectionArrays<FLOAT_TYPE, BATCH_SIZE>& targetData);
    bool compareData(const PhotonData& photonData, const VECTYPE::PhotonReflection<FLOAT_TYPE, BATCH_SIZE>& vecData, const size_t itemID);
    bool validateAllData();
    
public:
    //The vector containing the photon data which is generated as an array of structures
    //This is empty in case m_keepAOSData is false
    PhotonData::Vector m_AOSData;
    
    //Vector containing the SOA shuffled data
    VECTYPE::PhotonReflections<FLOAT_TYPE, BATCH_SIZE> m_VecData  __attribute__((__aligned__(64)));    
    
    //Data Structure containing data in a complete SOA structure. I.e. all x coordinates in a row,
    //all y coordinates in one row, etc.
    VECTYPE::PhotonReflectionArrays<FLOAT_TYPE, BATCH_SIZE> m_FullSOADAta; 
    
private:
    //To determine if we keep the original data around after generation. Use
    //to validate data processed by vectorized and normal path
    //Set to false to reduce memory consumption
    bool m_useAOSData;
    
    bool m_useHalfSOAData;
    
    bool m_useFullSOAData;
    
    //The number of photons to generate
    uint64_t m_nPhotons;
    
    //Histogram of deviation for original algorithm and ours
    std::vector<unsigned int> m_errorHisto;
    
};

#endif	/* RANDOMDATAGENERATOR_H */

