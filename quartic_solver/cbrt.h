#ifndef _CBRT_H_
#define _CBRT_H_

#include "vectorclass512.h"
#include <cmath>
#include <cstdint>
#include <tuple>

namespace MyRoots {
    inline std::tuple<float, float> cbrt(float x0, float x1) noexcept
    {
        const float xa = std::abs(x0);
        const float xb = std::abs(x1);
        int32_t i = reinterpret_cast<const int32_t&>(xa);
        int32_t j = reinterpret_cast<const int32_t&>(xb);
        i = 0x2a517d47 + i / 3;
        j = 0x2a517d47 + j / 3;
        float r = reinterpret_cast<float&>(i);
        float s = reinterpret_cast<float&>(j);
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        return std::make_tuple(std::copysign(r, x0), std::copysign(s, x1));
    }

    /** @brief calculate cube root
     *
     * @param x	number of which cube root should be computed
     * @returns cube root of x
     *
     * @author Manuel Schiller <Manuel.Schiller@cern.ch>
     * @date 2015-02-25
     */
    inline std::tuple<double, double> cbrt(double x0, double x1) noexcept
    {
        const double xa = std::abs(x0);
        const double xb = std::abs(x1);
        int64_t i = reinterpret_cast<const int64_t&>(xa);
        int64_t j = reinterpret_cast<const int64_t&>(xb);
        i = 0x2a9f84fe36d22425 + i / 3;
        j = 0x2a9f84fe36d22425 + j / 3;
        double s = reinterpret_cast<double&>(i);
        double r = reinterpret_cast<double&>(j);
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        return std::make_tuple(std::copysign(r, x0), std::copysign(s, x1));
    }

    // FIXME: Vec8d implementation
    inline Vec16f cbrt(const Vec16f x0) noexcept
    {
        const Vec16f xa = abs(x0);
        Vec16i i = Vec16i(reinterpret_i(xa));
        // FIXME: 
        i = 0x2a517d47 + i / 3;
        //i = 0x2a9f84fe36d22425 + i / 3;
        Vec16f r = reinterpret_f(i);
        r -= (r - xa / (r * r)) * (1.0f/ 3.0f);
        r -= (r - xa / (r * r)) * (1.0f/ 3.0f);
        r -= (r - xa / (r * r)) * (1.0f/ 3.0f);
        r -= (r - xa / (r * r)) * (1.0f/ 3.0f);
        // FIXME: something with 0xff..ff ?
        const Vec16f r_abs = abs(r);
        return select(sign_bit(x0), -r_abs, r_abs);
    }
}


#ifdef DEBUG_CBRT
#define ITERATIONS        1E8
int main ( int /*argc*/, char** /*argv*/ )
{
    int ret = 0;
    std::tuple<double, double> res;

    for (int i = 0; i < ITERATIONS; i +=2) {
        res = FastRoots::foo((double)i, (double)(i+1));
    }
    return (int)res[0];
}
#endif

#endif  /* _CBRT_H_ */
