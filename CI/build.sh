#!/bin/bash

set -e
set -x

BUILD_DIR=${1}

cd "$BUILD_DIR"
make clean || make -j 64 all
