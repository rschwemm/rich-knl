import math
import numpy as np

nvec = [-0.098053, -0.995087, 0.0136824, 0]
asinbeta = 0.103452
tmp = [846.569, 12.0234, 6941.27]
tmp3 = [1040.05,14.7713,8527.7]
sphReflPoint = [-189.395,1.59264,11898.5]

class Transform:
#    evecX, evecY, evecZ, dvecX, dvecY, dvecZ, CoCX, CoCY, CoCZ, sinbeta, radius, e;
    def __init__(self, evec, dvec, CoC, sinbeta, radius, e):
        self.evec = evec;
        self.dvec = dvec;
        self.CoC = CoC;

        self.evecX = evec[0];
        self.evecY = evec[1];
        self.evecZ = evec[2];
        self.dvecX = dvec[0];
        self.dvecY = dvec[1];
        self.dvecZ = dvec[2];
        self.CoCX = CoC[0];
        self.CoCY = CoC[1];
        self.CoCZ = CoC[2];
        self.sinbeta = sinbeta;
        self.radius = radius;
        self.e = e;

    def transform_linalg(self):
        nx = (self.evecY*self.dvecZ) - (self.evecZ*self.dvecY);
        ny = (self.evecZ*self.dvecX) - (self.evecX*self.dvecZ);
        nz = (self.evecX*self.dvecY) - (self.evecY*self.dvecX);

        norm = (nx*nx+ny*ny+nz*nz);
        divnorm = 1.0/norm;
        norm_sqrt = math.sqrt(norm);
        nx *= divnorm;
        ny *= divnorm;
        nz *= divnorm;

        print("n_xyz", nx, ny, nz)
        print("should be ", nvec)

        beta = math.asin(self.sinbeta);
        print("asinbeta", beta)
        print("should be ", asinbeta)

        a = self.sinbeta*norm_sqrt;
        b = (1.0-math.cos(beta))*(norm);
        # symmetric matrix, diagonal part is 0
        M = [   1+b*(-nz*nz-ny*ny),     a*nz+b*nx*ny,           -a*ny+b*nx*nz,
                -a*nz+b*nx*ny,          1+b*(-nx*nx-nz*nz),     a*nx+b*ny*nz,
                a*ny+b*nx*nz,           -a*nx+b*ny*nz,          1+b*(-ny*ny-nx*nx)];

#        M = [   1+b*(-nz*nz-ny*ny),    -a*nz+b*nx*ny,           a*ny-b*nx*nz,
#                a*nz+b*nx*ny,           1+b*(-nx*nx-nz*nz),     -a*nx+b*ny*nz,
#                -a*ny+b*nx*nz,          a*nx+b*ny*nz,           1+b*(-ny*ny-nx*nx)];

        ex = (self.evecX*M[0]+self.evecY*M[3]+self.evecZ*M[6]);
        ey = (self.evecX*M[1]+self.evecY*M[4]+self.evecZ*M[7]);
        ez = (self.evecX*M[2]+self.evecY*M[5]+self.evecZ*M[8]);
        enorm = self.radius/self.e;

        print("e_xyz", ex, ey, ez)
        print("should be ", tmp)
        print("mit norm: ", enorm*ex, enorm*ey, enorm*ez)
        print("should be ", tmp3)

        reflPointX = enorm*ex + self.CoCX;
        reflPointY = enorm*ey + self.CoCY;
        reflPointZ = enorm*ez + self.CoCZ;
        print("reflpoint: ", reflPointX, reflPointY, reflPointZ)
        print("should be ", sphReflPoint)

        return [reflPointX, reflPointY, reflPointZ]

# begin quaternion, source: http://stackoverflow.com/questions/4870393/rotating-coordinate-system-via-a-quaternion
    def normalize(self, v, tolerance=0.00001):
        mag2 = sum(n * n for n in v)
        if abs(mag2 - 1.0) > tolerance:
            mag = math.sqrt(mag2)
            v = tuple(n / mag for n in v)
        return v

    def q_mult(self, q1, q2):
        w1, x1, y1, z1 = q1
        w2, x2, y2, z2 = q2
        w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
        x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
        y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
        z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
        return w, x, y, z

    def q_conjugate(self, q):
        w, x, y, z = q
        return (w, -x, -y, -z)

    def qv_mult(self, q1, v1):
        q2 = (0.0,) + v1
        return self.q_mult(self.q_mult(q1, q2), self.q_conjugate(q1))[1:]

    def axisangle_to_q(self, v, theta):
        v = self.normalize(v)
        x, y, z = v
        theta /= 2
        w = math.cos(theta)
        x = x * math.sin(theta)
        y = y * math.sin(theta)
        z = z * math.sin(theta)
        return w, x, y, z

    def transform_quaternion(self):
        n = np.cross(self.evec, self.dvec);
        print("n ", n);
        n /= math.sqrt( np.dot(n,n) );
        print("n norm ", n);
        beta = math.asin(self.sinbeta)
        r = self.axisangle_to_q(n, beta)

        v = self.qv_mult(r, self.evec)
        print("tmp: ", v)
        v = map(lambda elem : elem * (self.radius/self.e), v)
        print("tmp2: ", v)

        reflPoint = [a+b for a, b in zip(self.CoC, v)]
        print("reflP: ", reflPoint)
        return reflPoint

if __name__ == "__main__":
    ld = np.longdouble
    evec = [ld(1555.35),ld(-59.5222),ld(6817.28)]
    dvec = [ld(-1155.48),ld(180.209),ld(4825.55)]
    CoC = [ld(-1229.45),ld(-13.1787),ld(3370.84)]
    sinbeta = ld(0.103268)
    radius = ld(8590.9)
    e = ld(6992.71)
    sphReflPoint = [ld(-189.395),ld(1.59264),ld(11898.5)]


    t = Transform(tuple(evec), tuple(dvec), CoC, sinbeta, radius, e)
    x = t.transform_linalg()
    print(x)
    print("should be ", sphReflPoint);
    print("diff: ", [a-b for a, b in zip(x, sphReflPoint)]);
    y = t.transform_quaternion()
    print(y)
    print("should be ", sphReflPoint);
    print("diff: ", [a-b for a, b in zip(y, sphReflPoint)]);
    print("lina", x)
    print("quart", y)
    print("diff", [a-b for a,b in zip(y,x)] )
