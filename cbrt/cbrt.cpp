#ifndef _CBRT_H_
#define _CBRT_H_

#include <cmath>
#include <cstdint>
#include <tuple>
#include <iostream>

#define MAX_VECTOR_SIZE 512
#include "vectorclass.h"


namespace MyRoots {
    std::tuple<float, float> cbrt(float x0, float x1) noexcept
    {
        const float xa = std::abs(x0);
        const float xb = std::abs(x1);
        int32_t i = reinterpret_cast<const int32_t&>(xa);
        int32_t j = reinterpret_cast<const int32_t&>(xb);
        i = 0x2a517d47 + i / 3;
        j = 0x2a517d47 + j / 3;
        float r = reinterpret_cast<float&>(i);
        float s = reinterpret_cast<float&>(j);
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        return std::make_tuple(std::copysign(r, x0), std::copysign(s, x1));
    }

    std::tuple<float, float> cbrt2(float x0, float x1) noexcept
    {
        const float xa = std::abs(x0);
        const float xb = std::abs(x1);
        int32_t i = reinterpret_cast<const int32_t&>(xa);
        int32_t j = reinterpret_cast<const int32_t&>(xb);
        i = 0x2a517d47 + i * (1.0/ 3.0);
        j = 0x2a517d47 + j * (1.0/ 3.0);
        float r = reinterpret_cast<float&>(i);
        float s = reinterpret_cast<float&>(j);
        r -= (r - xa / (r * r)) * (1.0 / 3.0);
        s -= (s - xb / (s * s)) * (1.0 / 3.0);
        r -= (r - xa / (r * r)) * (1.0 / 3.0);
        s -= (s - xb / (s * s)) * (1.0 / 3.0);
        r -= (r - xa / (r * r)) * (1.0 / 3.0);
        s -= (s - xb / (s * s)) * (1.0 / 3.0);
        return std::make_tuple(std::copysign(r, x0), std::copysign(s, x1));
    }

    /** @brief calculate cube root
     *
     * @param x	number of which cube root should be computed
     * @returns cube root of x
     *
     * @author Manuel Schiller <Manuel.Schiller@cern.ch>
     * @date 2015-02-25
     */
    std::tuple<double, double> cbrt(double x0, double x1) noexcept
    {
        const double xa = std::abs(x0);
        const double xb = std::abs(x1);
        int64_t i = reinterpret_cast<const int64_t&>(xa);
        int64_t j = reinterpret_cast<const int64_t&>(xb);
        i = 0x2a9f84fe36d22425 + i / 3;
        j = 0x2a9f84fe36d22425 + j / 3;
        double s = reinterpret_cast<double&>(i);
        double r = reinterpret_cast<double&>(j);
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        s -= (s - xb / (s * s)) / 3;
        r -= (r - xa / (r * r)) / 3;
        return std::make_tuple(std::copysign(r, x0), std::copysign(s, x1));
    }

/*    Vec8d cbrt(const Vec8d x0) noexcept
    {
        const Vec8d xa = abs(x0);
        Vec8uq i = Vec8uq(reinterpret_i(xa)/3);
        // FIXME: 
        i = 0x2a9f84fe36d22425 + i ;
        Vec8d r = reinterpret_d(i);
        r -= (r - xa / (r * r)) / 3;
        r -= (r - xa / (r * r)) / 3;
        r -= (r - xa / (r * r)) / 3;
        r -= (r - xa / (r * r)) / 3;
        // FIXME: something with 0xff..ff ?
        const Vec8d r_abs = abs(r);
        return select(sign_bit(x0), -r_abs, r_abs);
    }
*/
    Vec16f cbrt(const Vec16f x0) noexcept
    {
        const Vec16f xa = abs(x0);
        Vec16i i = Vec16i(reinterpret_i(xa));
        // FIXME: 
        i = 0x2a517d47 + i / 3;
        //i = 0x2a9f84fe36d22425 + i / 3;
        Vec16f r = reinterpret_f(i);
        r -= (r - xa / (r * r)) / 3;
        r -= (r - xa / (r * r)) / 3;
        r -= (r - xa / (r * r)) / 3;
        // FIXME: something with 0xff..ff ?
        const Vec16f r_abs = abs(r);
        return select(sign_bit(x0), -r_abs, r_abs);
    }
}

#ifdef DEBUG_CBRT

#define ITERATIONS        64
int main ( int /*argc*/, char** /*argv*/ )
{
    float x = 6.0, y = 5.5;
    std::tuple<float, float> res0 = MyRoots::cbrt(x,y);
    std::tuple<float, float> res1 = MyRoots::cbrt2(x,y);
    std::cout << std::get<0>(res0) << std::endl;
    std::cout << std::get<0>(res1) << std::endl;
#ifdef VEC8
#define N 8
    Vec8d vec{0.0f,1.0f,2.0f,3.0f,4.0f,5.0f,6.0f,7.0f};
    Vec8d tmpvec{0.0f,1.0f,2.0f,3.0f,4.0f,5.0f,6.0f,7.0f};
    Vec8d resv{0.0f,1.0f,2.0f,3.0f,4.0f,5.0f,6.0f,7.0f};
#else
#ifdef VEC16
#define N 16
    Vec16f vec{0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0};
    Vec16f tmpvec{0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0};
    Vec16f resv{0.0};
#endif /** VEC16*/
#endif /** VEC8*/
#ifdef N
    for (int i = 0; i < ITERATIONS/N; ++i) {
        resv = MyRoots::cbrt(tmpvec);
        tmpvec += N;
        std::cout << tmpvec[N-1] << std::endl;
    }
    std::cout << resv[N-1] << std::endl;
    return resv[N-1];
#else
#define TYPE float 
    int ret = 0;
    std::tuple<TYPE, TYPE> res;
    std::tuple<TYPE, TYPE> res2;
    std::cout << "---" << std::endl;
    for (TYPE i = 1035345.3423f, j = 0; j < ITERATIONS; j+=2, i *=4.324543435f) {
        std::cout <<  i << ", " << i+1;
    }
    std::cout << std::endl <<  "---" << std::endl;
    for (TYPE i = 1035345.3423f, j = 0; j < ITERATIONS; j+=2, i *=4.324543435f) {
        res = MyRoots::cbrt((TYPE)i, (TYPE)(i+1));
        res2 = MyRoots::cbrt2((TYPE)i, (TYPE)(i+1));
        std::cout << "cbrt: \t" << std::get<0>(res) << ", " << std::get<1>(res) << ", " << std::endl;
        std::cout << "cbrt2: \t" << std::get<0>(res2) << ", " << std::get<1>(res2) << ", "<< std::endl;
    }
    std::cout << std::endl;
    std::cout << std::get<1>(res) << std::endl;
    return (int)std::get<0>(res);
#endif /** N */
}
#endif /** DEBUG_CBRT */

#endif  /* _CBRT_H_ */
